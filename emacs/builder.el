;;;; Builder.el --- Compile lisp code and build it into emacs -*- lexical-binding: t; -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; Build the init.el from the modules.

;;; Code:
(defun init.el--add-file (filename)
  "Add FILENAME to init.el."
  (with-temp-buffer
    (insert (concat ";;;;; -- " filename "\n"))
    (insert (concat ";;* " (file-name-base filename)))
    (insert-file-contents filename)
    (insert "\n\n")
    (append-to-file (point-min) (point-max) "build/init.el")))


(defun init.el--build ()
  "Build the init.el file."
  ;; add header
  (with-temp-buffer
    (insert ";;;; init.el --- Emacas init script -*- lexical-binding: t; -*-\n\n")
    (insert (format-time-string ";;; Compiled at %F %X\n"))
    (insert ";;  _   _ _                     _       _\n"
	    ";; | |_(_) |_ _   _ ___   _ __ (_)_ __ | |_ __ _\n"
	    ";; | __| | __| | | / __| | '_ \\| | '_ \\| __/ _` |\n"
	    ";; | |_| | |_| |_| \\__ \\_| |_) | | | | | || (_| |\n"
	    ";;  \\__|_|\\__|\\__,_|___(_) .__/|_|_| |_|\\__\\__,_|\n"
	    ";;                       |_|\n")
    (insert "\n\n")
    (insert ";; Copyright (C) 2023 Titus Pinta\n"
	    ";; SPDX-License-Identifier: GPL-3.0-or-later\n"
	    ";; Part of https://gitlab.com/titus.pinta/config\n"
	    ";; See LICENSE.md for the license\n"
	    ";; See end of file for more info\n")

    (insert (format-time-string "(defvar init.el-compiled-at \"%F %X\"
  \"Timestamp for the compilation of the init file.\")\n"))
    (write-region (point-min) (point-max) "build/init.el"))


  (init.el--add-file "bootstrap.el")

  (init.el--add-file "modules/--base.el")
  (init.el--add-file "modules/--evil.el")
  (init.el--add-file "modules/--org.el")
  (init.el--add-file "modules/--completion.el")
  (init.el--add-file "modules/--theme.el")
  (init.el--add-file "modules/--dired.el")
  (init.el--add-file "modules/--ui.el")
  (init.el--add-file "modules/--edit.el")
  (init.el--add-file "modules/--navigation.el")
  (init.el--add-file "modules/--modeline.el")
  (init.el--add-file "modules/--tabbar.el")
  (init.el--add-file "modules/--git.el")
  (init.el--add-file "modules/--term.el")
  (init.el--add-file "modules/--project.el")
  (init.el--add-file "modules/--ide.el")

  ;; desktop
  (init.el--add-file "modules/desktop/--desktop.el")

  ;; langs
  (init.el--add-file "modules/langs/--web.el")
  (init.el--add-file "modules/langs/--latex.el")
  (init.el--add-file "modules/langs/--c.el")
  (init.el--add-file "modules/langs/--elisp.el")
  (init.el--add-file "modules/langs/--julia.el")
  (init.el--add-file "modules/langs/--fp.el")
  (init.el--add-file "modules/langs/--go.el")

  ;; agenda
  (init.el--add-file "modules/agenda/--email.el")


  (init.el--add-file "utils/--utils.el")
  (init.el--add-file "utils/--color.el")


  ;; add footer
  (with-temp-buffer
    (insert "(provide 'init)\n")
    (insert ";;; init.el ends here\n")
    (append-to-file (point-min) (point-max) "build/init.el"))

  ;; expand the debug macro
  ;; TODO when I figure out
  )

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide 'builder)
;;; builder.el ends here
