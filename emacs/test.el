;; latex command adder
;; \overline{f(x)}


(add-hook 'vterm-mode-hook  #'with-editor-export-editor)
(with-editor-async-shell-command "crontab -e")
(async-shell-command "crontab -e")


(define-key LaTeX-math-mode-map [remap LaTeX-math-phi] #'LaTeX-math-varphi)
(setq show-paren-when-point-inside-paren t)
