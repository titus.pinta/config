;;;; --utils.el --- Generic utility functions -*- lexical-binding: t; -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPLv3
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; All the utility functions that do not fit in a different file.

;;; Code:
(declare-function straight-use-package "straight.el")
(require 'cl-lib)

(defun create-scratch-buffer (num)
     "(Re)create the scratch buffer, using NUM to mangle the name."
     (interactive "p")
     (if (eq num 1)
	 (switch-to-buffer (get-buffer-create "*scratch*"))
       (switch-to-buffer (get-buffer-create (format "*scratch<%d>*" num))))
     (lisp-interaction-mode)
     (when (zerop (buffer-size))
       (insert (substitute-command-keys initial-scratch-message))))


(defun create-scratch-math (title)
  "Create a scratch math file with TITLE, using the timestamp to mangle the name."
  (interactive "sTitle: ")
  (find-file (format  "~/Notes/Scratch/%s.org"
		      (format-time-string "%y%m%d-%H%M")))
  (insert (format "#+author: Titus Pinta
#+:title: %s"
		  title)))

(straight-use-package 'uuid)
(declare-function uuid-string "uuid.el")
(defun insert-uuid ()
  "Insert UUID string at current cursor position."
  (interactive)
  (insert (uuid-string)))


;; utility functions for the info string in the tab bar
(declare-function f-read-text "f.el")
(advice-add 'f-read-text :override
	    (lambda (file) (shell-command-to-string (concat "cat " file))))
(defun cpu-freq ()
  "Return the cpu frequency."
  (* (string-to-number
      (f-read-text "/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq"))
     1024))

(defun cpu-perc ()
  "Return the cpu usage percentage."
  (cl-reduce
   #'+ (mapcar #'string-to-number
	       (cdr (split-string
		     (shell-command-to-string "ps -A -o pcpu") "\n")))))

;; the built in funxction memory-info does not work as I want
(defun memory--read-/proc/memionfo (type)
  "Read the amount of memoery in kB from /proc/meminfo coresponding to TYPE."
  (let* ((meminfo (f-read-text "/proc/meminfo")))
    (if (string-match (concat type ": *\\([-0-9]+\\) kB") meminfo)
	(string-to-number (match-string 1 meminfo))
      (error "Can not read /proc/meminfo"))))

(defun memory-used ()
  "Return the amount of used memory."
  (let ((total (memory--read-/proc/memionfo "MemTotal"))
	(free (memory--read-/proc/memionfo "MemFree"))
	(buffers (memory--read-/proc/memionfo "Buffers"))
	(cached (memory--read-/proc/memionfo "Cached")))
    (* (- total free buffers cached) 1024)))

(defun active-network-interface ()
  "Return the name of the active network interface."
  (let ((ip-string (shell-command-to-string "ip link show")))
    (if (string-match "[0-9]*: \\([a-z0-9]*\\):.* UP " ip-string)
	(match-string 1 ip-string)
      "")))

(defun wifi-ssid ()
  "Return the SSID of the of the connected wifi network."
  (let* ((dev-string (shell-command-to-string "iw dev"))
	 (dev (if (string-match "Interface \\(.*\\)\\>" dev-string)
		  (match-string 1 dev-string)))
	 (ssid-string (shell-command-to-string (concat "iw dev " dev " link"))))
    (if (string-match "SSID: \\(.*\\)\\>" ssid-string)
	(match-string 1 ssid-string)
      (error "wifi-ssid error"))))

(defun battery-status ()
  "Return the status of the battery."
  (shell-command-to-string "cat /sys/class/power_supply/BAT0/status"))

(defun battery-perc ()
  "Return the percentage of battery charge."
  (string-to-number
   (shell-command-to-string "cat /sys/class/power_supply/BAT0/capacity")))

(defun slock ()
  "Use slock to lock the screen."
  (interactive)
  (shell-command "slock"))

;; Profiling and instrumentation
(defun instrument-message-args-stop (symb)
  "Stop printing the arguments passed to the function SYMB when called."
  (advice-remove symb "instrument-message-args"))

(defun instrument-message-args (symb)
  "Print the arguments passed to the function SYMB when called."
  (advice-add symb
	      :before (lambda (&optional args)
		       (message "%s called with %s" symb args))
	      '((name . "instrument-message-args"))))
;; (trace-function-background 'indent-for-tab-command)

;; TODO toggle and default value for enable
(defun enable-pointing-devices (enable)
  "Control the pointing devices, using xinput.

If ENABLE is non-nil enable the mouse and touch pad, otherwise disable them."
  (interactive "sEnable: ")
  (mapcar
   (lambda (id)
     (shell-command-to-string (concat "xinput --" enable " " id)))
   (with-temp-buffer
     (insert (shell-command-to-string "xinput"))
     (goto-char (point-min))
     (let ((ids ()))
       (while (search-forward-regexp
	       "id=\\([0-9]*\\)\t\\[slave  pointer  ([0-9]*)\\]"
	       nil t)
	 (push (match-string 1) ids))
       ids))))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--utils)
;;; --utils.el ends here
