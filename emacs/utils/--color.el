;;;; --color.el --- Utility functions for dealing with colors -*- lexical-binding: t; -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; Manipulate textual representation of colors

;;; Code:
(require 'color)
(defun hex-to-rgb (hex)
  "Convert color HEX from \"#RRGGBB\" to list of red grean blue 8-bit values."
  (let ((rrggbb (cond
		 ((string= (substring hex 0 1) "#") (substring hex 1 7))
		 ((string= (substring hex 0 2) "0x") (substring hex 2 8))
		 (t hex))))
    (list
     (string-to-number (substring rrggbb 0 2) 16)
     (string-to-number (substring rrggbb 2 4) 16)
     (string-to-number (substring rrggbb 4 6) 16))))

(defun rgb-to-hex (rgb)
  "Convert color RGB of red grean blue (8-bit or float) to \"RRGGBB\"."
  (pcase (pcase rgb
	   ((pred (seq-every-p #'integerp)) rgb)
	   ((and (pred (seq-every-p (apply-partially #'<= 0)))
		 (pred (seq-every-p (apply-partially #'>= 1))))
	    (mapcar #'round (mapcar (apply-partially #'* 255) rgb))))
    (`(,r ,g, b)
     (format "#%02X%02X%02X" r g b))
    (_ (error "Argument not in the correct format"))))

(defun change-lightnes-hex-color (hex transform)
  "Apply TRANSFORM to the third component in the hsl representation of HEX.

Return the hex representation of the new color."
  (let ((rgb (hex-to-rgb hex)))
    (let ((hsl (color-rgb-to-hsl (/ (car rgb) 255.0)
				 (/ (cadr rgb) 255.0)
				 (/ (caddr rgb) 255.0))))
      (rgb-to-hex (color-hsl-to-rgb (car hsl)
				    (cadr hsl)
				    (funcall transform
					     (caddr hsl)))))))

(defun darken-color (hex)
  "Return hex representation of a color darker than HEX."
  (change-lightnes-hex-color hex (apply-partially '+ -0.0065)))
(defun lighten-color (hex)
  "Return hex representation of a color lighter than HEX."
  (change-lightnes-hex-color hex (apply-partially '+  0.0435)))


(require 'ansi-color)

(defun ansi-colorize-buffer ()
  "Colorize ansi escape sequances in buffer."
  (interactive)
  (ansi-color-apply-on-region (point-min) (point-max)))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--color)
;;; --color.el ends here
