;; do something like harpoon

(defvar buffer-ring '())

(defun buffer-ring-add-buffer ()
  (interactive)
  (add-to-list 'buffer-ring (current-buffer)))

()

(defun buffer-ring-switch ()
  (read-buffer "Switch to buffer: " nil nil
	       (lambda (buffer) (member (cdr buffer) buffer-ring))))

