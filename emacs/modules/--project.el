;;;; --project.el --- Configuration for project management -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; Provide the configuration for the build in module project.el

;;; Code:
(global-set-key (kbd "C-x p C-b") 'consult-project-buffer)

;; install the current project, using 'compile
;; the contents of the compile command variable are saved in order to be
;; used later by other commands
(defvar project-compile-install-command "make -k install"
  "The command to be run by \='project-compile-install'.")

(declare-function project-root "project.el")
(defun project-compile-install ()
  "Run make install on the current project."
  (declare (interactive-only compile))
  (interactive)
  (let ((default-directory (project-root (project-current t)))
	(--compile-command compile-command))
    (setq compile-command project-compile-install-command)
    (call-interactively #'compile)
    (setq compile-command --compile-command)))

(global-set-key (kbd "C-x p i") 'project-compile-install)

;; test the current project using compile TODO
(defvar project-compile-test-command "make -k test"
  "The command to be run by \='project-compile-test'.")

(defun project-compile-test ()
  "Run make test on the current project."
  (declare (interactive-only compile))
  (interactive)
  (let ((default-directory (project-root (project-current t)))
	(--compile-command compile-command))
    (setq compile-command project-compile-test-command)
    (call-interactively #'compile)
    (setq compile-command --compile-command)))

(global-set-key (kbd "C-x p t") 'project-compile-test)

;; hide the compilation buffer
(add-hook 'compilation-finish-functions
	  (lambda (buf str)
	    (unless (string-match ".*exited abnormally.*" str)
	      (quit-windows-on buf nil))))

(defun project-open-todo ()
  "Open todo file in the project root."
  (interactive)
  (let ((default-directory
	  (file-name-as-directory (project-root (project-current t)))))
    (cond
     ((file-exists-p (concat default-directory "todo.org"))
      (find-file-other-window (concat default-directory "todo.org")))
     ((file-exists-p (concat default-directory "todo.txt"))
      (find-file-other-window (concat default-directory "todo.txt")))
     ((yes-or-no-p "Create TODO file?")
      (find-file-other-window (concat default-directory "todo.org"))))))
(keymap-unset project-prefix-map "o")
(keymap-set project-prefix-map "o t" 'project-open-todo)

(setq-default project-switch-commands #'project-find-dir)

  ;; manage license file
(declare-function f-file-p "f.el")
(eval-when-compile (require 'rx))
(defun project--get-license-type ()
  "Return the type of the current license file."
  (if (f-file-p (concat (project-root (project-current)) "LICENSE.md"))
      (with-temp-buffer
	(insert-file-contents
	 (concat (project-root (project-current)) "LICENSE.md"))
	(pcase (buffer-substring-no-properties (point-min) (point-max))
	  ((rx "# GNU GENERAL PUBLIC LICENSE\n\nVersion 3, 29 June 2007\n")
	   "GPL-3.0-or-later")
	  )
	)
    nil))

(defun project-get-license-type ()
  "Message the type of the current license file."
  (interactive)
  (message (project--get-license-type)))

(defun project-license (type)
  "Manage the project license file.

Returns the type of the current license file.
When TYPE is non-nil adds a TYPE license file."
  ;; get the current license type
  (interactive
   (list (read-string
	  (format "(Current: %s) New License Type: "
		  (project-get-license-type)))))
  (url-copy-file
   (pcase type
     ("BSD" "https://raw.githubusercontent.com/IQAndreas/markdown-licenses/master/bsd-3.md")
     (_ "https://www.gnu.org/licenses/gpl-3.0.md"))
   (concat (project-root (project-current)) "LICENSE.md")))

;; finding files in projects if current directory is in a project
(defun find-file-or-project-find-file ()
  "Run \='project-find-file' if in a project, else run \='find-file'."
  (interactive)
  (if (project-current)
      (call-interactively #'project-find-file)
    (call-interactively #'find-file)))

;; (global-set-key [remap find-file] #'find-file-or-project-find-file)
(global-set-key (kbd "C-x C-f") #'find-file-or-project-find-file)
;; (global-set-key [remap project-find-file] #'project-or-external-find-file)
(global-set-key (kbd "C-x f") 'find-file)

(provide '--project)
;;; --project.el ends here
