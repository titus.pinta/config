;;;; --base.el --- Configuration of base built-in functionality -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; Set up the most basic behaviour of Emacs, like inhibiting the start up
;; message and removing the tool bars.

;;; Code:
;; time start up
(add-hook 'emacs-startup-hook
	  (lambda () ;; after start up, the garbage collector has to run more often
	    (setq gc-cons-threshold (eval-when-compile (* 10 1024 1024)))
	    (message "Number of garbage collections %d" gcs-done)
	    (message "Started in %s" (emacs-init-time))))

;; stop the garbage collector from running to often
(setq gc-cons-threshold (eval-when-compile (* 1024 1024 1024)))
(run-with-idle-timer 2 t (lambda () (garbage-collect)))

(setq disabled-command-function nil)

(setq inhibit-startup-message 1)

;; remove extra display elements
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(menu-bar-mode -1)

(setq visible-bell 1)

;; wrap lines
(setq-default truncate-lines nil)
(setq-default word-wrap nil)

;; maximize frame on startup
(setq frame-resize-pixelwise 1)
(custom-set-variables
 '(initial-frame-alist (quote ((fullscreen . maximized)))))

(customize-set-variable 'global-auto-revert-non-file-buffers 1) ;revert dired
(global-auto-revert-mode 1)

;; replace yes/no with y/n
(setq use-short-answers 1)

;; start the daemon
(unless (and (boundp 'init.el-compiling) init.el-compiling)
  (unless (ignore-errors (server-mode 1))
    (message "Server startup error")))

;; emacsclient should use new tabs
(defvar server-window)
(setq server-window #'switch-to-buffer-other-tab)

;; collaborative editing
(declare-function straight-use-package "straight.el")
(straight-use-package 'crdt)

(add-hook 'after-init-hook #'recentf-mode)

;; move autosave files (#*#) into .cache
(setq auto-save-file-name-transforms
      '((".*" "~/.cache/emacs/" t)))

;; move backups (*~) into .cache
(setq backup-directory-alist
      '(("~" . "~/.cache/emacs/")
	("." . "~/.cache/emacs/")))

;; burry buffer
(global-set-key (kbd "C-x K") #'bury-buffer)

;; recursive minibuffer allow evaluation of lisp forms in a minibuffer
(setq enable-recursive-minibuffers t)

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--base)
;;; --base.el ends here
