;;;; --navigation.el --- Easy navigation between, and inside windows -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; Set up fast navigation inside a buffer and between windows using decision
;; trees.
;;; Code:
;; ace navigation
(declare-function straight-use-package "straight.el")
(straight-use-package 'ace-window)
;; only use the visible frames for ace
(setq aw-scope 'visible)
(declare-function ace-window "ace-window.el")
(global-set-key (kbd "C-x w") #'ace-window)

(when (featurep '--evil)
  (defvar evil-normal-state-map)
  (define-key evil-normal-state-map (kbd "<SPC> w") #'ace-window))

(with-eval-after-load 'ace-window
  (defvar aw-keys)
  (setq aw-keys '(?j ?k ?l ?a ?s ?d ?f ?g ?h)))

(straight-use-package 'avy)
(declare-function avy-goto-char "avy.el")
(declare-function avy-goto-line "avy.el")
(declare-function avy-goto-word-1 "avy.el")
(global-set-key (kbd "C-<return> f") #'avy-goto-char)
(global-set-key (kbd "C-<return> j c") #'avy-goto-char)
(global-set-key (kbd "C-<return> j l") #'avy-goto-line)
(global-set-key (kbd "C-<return> j w") #'avy-goto-word-1)


(when (featurep '--evil)
  (defvar evil-normal-state-map)
  (define-key evil-normal-state-map (kbd "<SPC> f") #'avy-goto-char)
  (define-key evil-normal-state-map (kbd "<SPC> j c") #'avy-goto-char)
  (define-key evil-normal-state-map (kbd "<SPC> j l") #'avy-goto-line)
  (define-key evil-normal-state-map (kbd "<SPC> j w") #'avy-goto-word-1))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--navigation)
;;; --navigation.el ends here
