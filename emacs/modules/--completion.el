;;;; --completion.el --- Configuration for completion tools -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; Provide the configuration improving the auto completion in Emacs.
;; It uses the modern packages from the vertico familly to provide
;; completion functionality in a way compatible with the built in Emacs
;; behavior.

;; The packages used are =vertico= (with =marginalia= and =orderless=),
;; =consult=, =corfu= and =embark=.

;;; Code:

;;: editing in the minibuffer:
;; vim like keybindings to edit in the minibuffer
(define-key minibuffer-local-map (kbd "C-h") #'delete-backward-char)
(define-key minibuffer-local-map (kbd "M-h") #'backward-kill-word)
(define-key minibuffer-local-map (kbd "C-l") #'exit-minibuffer)
(define-key minibuffer-local-map (kbd "<tab>") #'completion-at-point)

;; * Vertico:
;; use vertico for minibuffer completion
(declare-function straight-use-package "straight.el")

(straight-use-package 'vertico)
(declare-function vertico-mode "vertico.el")
(vertico-mode 1)

;; cycle through minibuffer options
(defvar vertico-cycle)
(setq vertico-cycle t)

;; use vim style keys to navigate through minibuffer
(with-eval-after-load 'vertico
  (defvar vertico-map)
  (declare-function vertico-exit "vertico.el")
  (define-key vertico-map (kbd "C-l") #'vertico-exit)
  (declare-function vertico-next "vertico.el")
  (define-key vertico-map (kbd "C-j") #'vertico-next)
  (declare-function vertico-previous "vertico.el")
  (define-key vertico-map (kbd "C-k") #'vertico-previous)
  ;; use tab to complete, not to insert \t
  (declare-function vertico-insert "vertico.el")
  (define-key vertico-map (kbd "<tab>") #'vertico-insert)

  ;; vertico extensions
  ;; ace style quick jumping
  (require 'vertico-quick)
  (define-key vertico-map (kbd "M-l") 'vertico-quick-jump))

(require 'savehist)
(savehist-mode 1)


;;; Orderless:
;; use the orderless completion style
;; this matches PATTERNS separated by a space
(straight-use-package 'orderless)
(setq completion-styles '(orderless basic)
      completion-category-defaults nil
      completion-category-overrides '((file (styles partial-completion))))

;; * Marginalia:
;; add useful annotation to the minibuffer entries
(straight-use-package 'marginalia)
(with-eval-after-load 'vertico
  (declare-function marginalia-mode "marginalila.el")
  (marginalia-mode 1))

;; * Consult:
;; replace buit in commands with consult (vertico based)
(straight-use-package 'consult)
(with-eval-after-load 'consult
  (require 'consult-imenu)
  (with-eval-after-load 'flymake
    (require 'consult-flymake)))

(declare-function consult-buffer "consult")
(global-set-key (kbd "C-x C-b") #'consult-buffer)
(global-set-key (kbd "C-x i") #'ibuffer)

(declare-function consult-line "consult")
(global-set-key (kbd "C-s") #'consult-line) ; like ivy-swiper
(declare-function consult-line-multi "consult")
(global-set-key (kbd "C-c c m s") #'consult-line-multi)

(declare-function consult-yank-from-kill-ring "consult")
(global-set-key (kbd "C-c c y") #'consult-yank-from-kill-ring)

(declare-function consult-recent-file "consult")
(global-set-key (kbd "C-c c f") #'consult-recent-file)
(declare-function consult-grep "consult")
(global-set-key (kbd "C-c c G") #'consult-grep)
(declare-function consult-git-grep "consult")
(global-set-key (kbd "C-c c g") #'consult-git-grep)
(declare-function consult-ripgrep "consult")
(global-set-key (kbd "C-c c r g") #'consult-ripgrep)

(declare-function consult-imenu "consult")
(global-set-key (kbd "C-c c i") #'consult-imenu)
(declare-function consult-mark "consult")
(global-set-key (kbd "C-c c SPC") #'consult-mark)

(when (featurep '--evil)
  (declare-function evil-collection-consult-jump-list "evil-collection-consult")
  (global-set-key (kbd "C-c c j l") #'evil-collection-consult-jump-list)

  (declare-function evil-collection-consult-mark "evil-collection-consult")
  (global-set-key (kbd "C-c c m") #'evil-collection-consult-mark))

(declare-function consult-flymake "consult")
(global-set-key (kbd "C-c e") #'consult-flymake)

;; use consult to complete in minibuffer environments (like eval-expression)
(declare-function consult-completion-in-region "consult")
(add-hook 'minibuffer-setup-hook
	  (lambda () (setq completion-in-region-function
		      #'consult-completion-in-region)))

;; * Corfu:
;; a completion at point package
(straight-use-package 'corfu)
(declare-function global-corfu-mode "corfu")
(global-corfu-mode 1)

(setq tab-always-indent 'complete)

(setq completion-cycle-threshold 3)

(defvar corfu-cycle)
(setq corfu-cycle 1)

(defvar corfu-map)
;; use space as a separator in corfu for orderless
(declare-function corfu-insert-separator "corfu.el")
(define-key corfu-map (kbd "SPC") #'corfu-insert-separator)

(declare-function corfu-complete "corfu.el")
(define-key corfu-map (kbd "C-i") #'corfu-complete)
(define-key corfu-map (kbd "C-l") #'corfu-complete)

(when (featurep '--evil)
  (with-eval-after-load 'evil
    (declare-function evil-define-key* "evil-core.el")
    (evil-define-key* 'insert corfu-map (kbd "C-i") #'corfu-complete)
    (evil-define-key* 'replace corfu-map (kbd "C-i") #'corfu-complete)))

;; * Capes:
(straight-use-package 'cape)
(declare-function cape-dabbrev "cape")
(declare-function cape-abbrev "cape")
(declare-function cape-file "cape")
(declare-function cape-tex "cape-char")

;; ;; this needs to be a function because some mods override the global value
(defun add-capes-to-buffer ()
  "Add useful completion functions."
  (add-to-list 'completion-at-point-functions #'cape-dabbrev) ; from current buf
  (add-to-list 'completion-at-point-functions #'cape-abbrev) ; from custom abbrev
  (add-to-list 'completion-at-point-functions #'cape-file) ; file names
  (add-to-list 'completion-at-point-functions #'cape-tex)) ; unicode from tex

(add-capes-to-buffer)
(add-hook 'prog-mode-hook #'add-capes-to-buffer)
(add-hook 'text-mode-hook #'add-capes-to-buffer)

;; * Embark:
(straight-use-package 'embark)
(straight-use-package 'embark-consult)
(declare-function embark-act "embark")

(global-set-key (kbd "C-c SPC") #'embark-act)
(when (featurep '--evil)
  (with-eval-after-load 'evil
    (defvar evil-normal-state-map)
    (define-key evil-normal-state-map (kbd "<SPC> <SPC>") #'embark-act)))

(with-eval-after-load 'embark
  (declare-function only-in-debug nil)
  (only-in-debug (message "Debug: defer loading: embark-consult loaded"))
  (require 'embark-consult)
  (defvar embark-help-key "?"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--completion)
;;; --completion.el ends here
