;;;; --edit.el --- Generic text editing configuration -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; Set up the undo-redu system and the spell checking. This are required for
;; most general editting.

;;; Code:
;; one space after period
(setq sentence-end-double-space nil)

(declare-function straight-use-package "straight.el")
;; undo tree
(straight-use-package 'undo-tree)
(declare-function global-undo-tree-mode "undo-tree.el")
(declare-function undo-tree-undo "undo-tree.el")
(declare-function undo-tree-redo "undo-tree.el")

(when (featurep '--evil)
  (global-undo-tree-mode 1)
  (defvar evil-undo-system)
  (setq evil-undo-system 'undo-tree)
  (defvar evil-undo-function)
  (setq evil-undo-function #'undo-tree-undo)
  (defvar evil-redo-function)
  (setq evil-redo-function #'undo-tree-redo)
  (defvar evil-want-fine-undo)
  (setq evil-want-fine-undo t)
  (add-hook ’evil-local-mode-hook ’turn-on-undo-tree-mode))

;; save the undo=tree history in .cache
(defvar undo-tree-history-directory-alist)
(with-eval-after-load 'undo-tree
  (setq undo-tree-history-directory-alist '((".*" . "~/.cache/emacs"))))

;; autocorrect
(with-eval-after-load 'ispell
  (defvar ispell-program-name)
  (setq ispell-program-name "aspell")
  ;; (setq-default ispell-local-dictionary "english")
  (setq-default ispell-local-dictionary "american"))

(add-hook 'prog-mode-hook #'flyspell-prog-mode)
(add-hook 'text-mode-hook #'flyspell-mode)

(add-hook 'flyspell-mode-hook (lambda () (make-thread #'flyspell-buffer))) ;;TODO investigate
(add-hook 'flyspell-mode-hook #'ispell-minor-mode) ;;TODO investigate
(with-eval-after-load 'flyspell
  (defvar flyspell-mode-map)
  (define-key flyspell-mode-map (kbd "C-;") nil))

(electric-pair-mode 1)

(straight-use-package 'tiny)
(declare-function tiny-expand "tiny.el")

(straight-use-package 'iedit)
(declare-function iedit-mode "iedit.el")
(global-set-key (kbd "C-;") #'iedit-mode)

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--edit)
;;; --edit.el ends here
