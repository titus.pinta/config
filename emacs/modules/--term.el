;;;; --term.el --- Configuration for shells and terminal emulators -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; Make eshell look and behave like zsh. Provide modern terminal emulators to
;; be used by inferior REPLS

;;; Code:
(declare-function straight-use-package "straight.el")
(straight-use-package 'vterm)

;; load vterm when multi-vterm is used
(autoload 'multi-vterm "vterm.el")

;; TODO (cdaddr vterm-mode-map) is hard coded. Find a way to replace it
(with-eval-after-load 'vterm
  (defvar vterm-mode-map)
  (declare-function vterm-send-backspace "vterm.el")
  (define-key (cdaddr vterm-mode-map) (kbd "C-h") #'vterm-send-backspace)
  (declare-function evil-emacs-state "evil-states.el")
  (add-hook 'vterm-mode-hook #'evil-emacs-state)

  ;; open a comunication line to the shell in vterm

  ;; When vterm is opend from a project, cd to the root, using multi vterms
  (straight-use-package 'multi-vterm)
  (declare-function vterm "vterm.el")
  (declare-function project-root "project.el")
  (declare-function multi-vterm-project "multi-vterm.el")
  (declare-function vterm-send-string "vterm.el")
  (advice-add
   #'vterm :around
   (lambda (old &rest r)
     (if (project-current)
	 (multi-vterm-project)
       (apply old r))))

  (defun vterm-project-send-command (s)
    "Send S to the inferior vterm"
    (interactive "sCommand: ")
    (when (project-root (project-current t))
      (with-current-buffer (format "*vterminal - %s*"
				   (project-root (project-current t)))
	(vterm-send-string (format "%s\n" s)))))

  ;; change the working directory to match the shell directory, via pid
  ;; TODO for the future a more advance communication channels based
  ;; architecture should be used
  ;; (defvar vterm--process)
  ;; (advice-add
  ;;  'vterm--update :after
  ;;  (lambda (&rest _)
  ;;    "Set the working directory using the vterm pid."
  ;;    (setq-local default-directory
  ;; 		 (string-trim-left
  ;; 		  (cadr
  ;; 		   (split-string
  ;; 		    (shell-command-to-string
  ;; 		     (format "pwdx %d" (process-id vterm--process)))
  ;; 		    ":")))))))


(defun truncate-file-name (filename) ;; TODO
  "Truncate FILENAME."
  filename)

(with-eval-after-load 'esh-mode
  (defvar eshell-mode-map)
  (define-key eshell-mode-map (kbd "<tab>") 'completion-at-point)
  (define-key eshell-mode-map (kbd "C-k")
	      'eshell-previous-matching-input-from-input)
  (define-key eshell-mode-map (kbd "C-j")
	      'eshell-next-matching-input-from-input)
  (define-key eshell-mode-map (kbd "C-l") 'eshell-send-input)


  ;;  eshell prompt
  (defvar eshell-highlight-prompt)
  (setq eshell-highlight-prompt nil)

  (defvar eshell-last-command-status)
  (declare-function eshell/pwd "em-dirs.el")
  (declare-function vc-git--symbolic-ref "vc-git.el")

  (defvar eshell-prompt-function)
  (setq eshell-prompt-function
	(lambda ()
	  "Produce a prompt for eshell.

Looks like:
$?>$USER@$HOST:$(pwd)%"
	  (let
	      ((--prompt
		(concat
		 (propertize (number-to-string eshell-last-command-status)
			     'font-lock-face
			     `(:inherit
			       ,(if (= eshell-last-command-status 0)
				    'success 'error)
			       :weight normal))
		 ">"
		 (propertize user-login-name 'font-lock-face 'package-description)
		 "@"
		 (propertize (system-name) 'font-lock-face
			     '(:inherit font-lock-keyword-face :weight normal))
		 ":"
		 (propertize
		  (if (> (frame-width) 72)
		      (abbreviate-file-name (eshell/pwd))
		    (truncate-file-name (eshell/pwd)))
		  'font-lock-face font-lock-constant-face)
		 (when (eq (vc-responsible-backend (eshell/pwd) t) 'Git)
		   (concat ":"
			   (propertize (vc-git--symbolic-ref (eshell/pwd))
				       'font-lock-face
				       '(:inherit warning :weight normal))
			   ))
		 "% ")))
	    (add-text-properties 0 (length --prompt)
				 '(read-only t front-sticky t rear-nonsticky t)
				 --prompt)
	    --prompt)))

  (defvar eshell-prompt-regexp)
  (setq eshell-prompt-regexp "^[0-9]*>[^@]*@[^%]*% ")

  (straight-use-package 'eshell-syntax-highlighting)
  (require 'eshell-syntax-highlighting)
  (declare-function eshell-syntax-highlighting-global-mode
		    "eshell-syntax-highlighting.el")
  (eshell-syntax-highlighting-global-mode 1)

  (set-face-attribute 'eshell-syntax-highlighting-shell-command-face nil
		      :foreground 'unspecified :inherit 'success :weight 'normal)
  (set-face-attribute 'eshell-syntax-highlighting-invalid-face nil
		      :weight 'bold)

  ;; environment variables
  (setenv "GOBIN" "$HOME/.local/go")

  ;; aliases for eshell, matching zsh
  (declare-function f-file? "f.el")
  (declare-function eshell/alias "em/alias.el")
  (if (f-file? "/bin/eza-wrapper")
      (eshell/alias "ls"  "eza-wrapper")
    (eshell/alias "ls" "eza"))

  (eshell/alias "sudo" "doas")
  (eshell/alias "vim" "find-file")
  ;; (eshell/alias "less" "slit")
  ;; (eshell/alias "more" "slit")
  (eshell/alias 'emacsclient 'find-file))
;; open terms in an inferior repl
(add-to-list 'display-buffer-alist
	     '((lambda (buf _)
		 (or (with-current-buffer buf (derived-mode-p 'eshell-mode))
		     (with-current-buffer buf (derived-mode-p 'vterm-mode))))
	       . (display-buffer-in-side-window)))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--term)
;;; --term.el ends here
