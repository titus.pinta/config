;;;; --ide.el --- Configuare LSP support and other IDE features -*- lexical-binding: t -*-
;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; This is the file for general configuration for eglot.el, project.el
;; tree-sitter.el. All languages are loaded in order to provide a basic
;; usable interface for any language. Environment specific configuration for
;; particular languages is done in the individual files in the langs
;; directory. In these individual files, you can find primly the customize
;; build systems

;;; Code:
;; (define-derived-mode rust-mode
;;   prog-mode "Rust"
;;   "Major mode for rust.")
;; (add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode))
;; (define-derived-mode julia-mode
;;   prog-mode "Julia"
;;   "Major mode for julia.")
;; (add-to-list 'auto-mode-alist '("\\.jl\\'" . julia-mode))
;; (straight-use-package 'rust-mode)


(declare-function straight-use-package "straight.el")

;; * Configuration for eglot
(straight-use-package 'eglot)
(declare-function eglot-completion-at-point "eglot.el")
(add-hook 'eglot-connect-hook
	  (lambda () (add-to-list 'completion-at-point-functions
			     #'eglot-completion-at-point)))

;; * Configuration for eldoc
(add-hook 'eglot-managed-mode-hook
	  (lambda () (setq eldoc-documentation-strategy
		      'eldoc-documentation-compose-eagerly)))



;; * Configuration for flymake
(straight-use-package 'flymake)
(defvar menu-bar-project-menu ())
(defvar prog-mode-hook)
(declare-function flymake-mode "flymake.el")
(add-hook 'prog-mode-hook #'flymake-mode)


;; * Functions and keybindings to work with the inferior term
;; TODO replace project with ide
(declare-function project-root "project.el")
(declare-function multi-vterm-project "multi-vterm.el")
(declare-function vterm-project-send-command "init.el")

(defvar project-inferior-term-map (make-sparse-keymap)
  "Keynmap for controlling the inferior terminal.")
(global-set-key (kbd "C-c r") project-inferior-term-map)

(defvar-local project-run-command
    (lambda (cmd) (interactive "sCommand: ") cmd)
  "Function to generate the current run command.

The default value is to prompt the user")
(defun project-inferior-run ()
  "Run the current project, displaying in the inferior vterm buffer."
  (interactive)
  (when (project-root (project-current t))
    (project-inferior-show)
    (vterm-project-send-command (funcall-interactively project-run-command))))

(define-key project-inferior-term-map (kbd "r") #'project-inferior-run)
(define-key project-inferior-term-map
 (kbd "h")
 (lambda ()
   "Open inferior terminal."
   (interactive)
   (delete-window (get-buffer-window
		   (format "*vterminal - %s*"
			   (project-root (project-current t)))))))
(defun project-inferior-show ()
  "Display the inferior terminal."
  (interactive)
    (let ((buffer-name (format "*vterminal - %s*"
			       (project-root (project-current t)))))
      (unless (get-buffer buffer-name)
	(multi-vterm-project))
      (unless (get-buffer-window buffer-name)
	(display-buffer buffer-name))))
(define-key project-inferior-term-map (kbd "s") #'project-inferior-show)

(defun project-inferior-interactive ()
  "Start an interactive repl for the current project."
  ;; TODO this has to figure out based on the project how to do this
  ;; in julia it should use snail
  ;; in rust this could interact with the compiler
  )

(defun project)

;; * Configuring tree-sitter
(defvar treesit-language-source-alist)
(setq treesit-language-source-alist
      '((c . ("https://github.com/tree-sitter/tree-sitter-c"))
	(cpp . ("https://github.com/tree-sitter/tree-sitter-cpp"))
	(css . ("https://github.com/tree-sitter/tree-sitter-css" "v0.20.0"))
        (go . ("https://github.com/tree-sitter/tree-sitter-go" "v0.20.0"))
	(gomod "https://github.com/camdencheek/tree-sitter-go-mod")
        (html . ("https://github.com/tree-sitter/tree-sitter-html" "v0.20.1"))
        (javascript . ("https://github.com/tree-sitter/tree-sitter-javascript" "v0.20.1" "src"))
        (json . ("https://github.com/tree-sitter/tree-sitter-json" "v0.20.2"))
        (markdown . ("https://github.com/ikatyang/tree-sitter-markdown" "v0.7.1"))
        (python . ("https://github.com/tree-sitter/tree-sitter-python" "v0.20.4"))
        (rust . ("https://github.com/tree-sitter/tree-sitter-rust" "v0.21.2"))
        (toml . ("https://github.com/tree-sitter/tree-sitter-toml" "v0.5.1"))
        (tsx . ("https://github.com/tree-sitter/tree-sitter-typescript" "v0.20.3" "tsx/src"))
        (typescript . ("https://github.com/tree-sitter/tree-sitter-typescript" "v0.20.3" "typescript/src"))
        (yaml . ("https://github.com/ikatyang/tree-sitter-yaml" "v0.5.0"))))
(mapc (lambda (lang)
	(unless (treesit-language-available-p (car lang))
	  (treesit-install-language-grammar (car lang))))
      treesit-language-source-alist)

;; todo maybe write assertion tests here
;; (unless (--all? #'identitiy
;; 	 (mapcar #'treesit-language-available-p
;; 		 (mapcar #'car treesit-language-source-alist)))
;;   (error "Tree sitter error"))

(defvar major-mode-remap-alist)
(dolist (mapping
	 '((go-mode . go-ts-mode)
	   (rust-mode . rust-ts-mode)
	   (c-mode . c-ts-mode)))
  (add-to-list 'major-mode-remap-alist mapping))

(customize-set-value 'treesit-font-lock-level 4)
;; * Configuring combobulate

;; * Other usefule IDE features
;; ** Docker
(straight-use-package 'dockerfile-mode)

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--ide)
;;; --ide.el ends here
