;;;; --evil.el --- Configuration for evil mode -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; Configure vim like behaviour, particularly through evil mode.

;;; Code:
(declare-function straight-use-package "straight.el")
(straight-use-package 'evil)
(defvar evil-want-integration)
(setq evil-want-integration 1)
(defvar evil-want-C-u-scroll)
(setq evil-want-C-u-scroll t)
;; disable key bindings because it conflicts with evil collections
;; this is redundant with bootstrap.el
(defvar evil-want-keybinding)
(setq evil-want-keybinding nil)

(declare-function evil-mode "evil.el")
(evil-mode 1)

(defvar evil-normal-state-map)
(defvar evil-insert-state-map)
(defvar evil-replace-state-map)
(defvar evil-visual-state-map)

(define-key evil-insert-state-map (kbd "C-i") #'indent-for-tab-command)
(define-key evil-replace-state-map (kbd "C-i") #'indent-for-tab-command)
(define-key evil-visual-state-map (kbd "C-i") #'indent-for-tab-command)

;; in emacs (because of old stuff) ctrl i and tab are identical, so when evil
;; binds evil-jump-forward to ctrl i, it is also bound to tab. (kbd <tab>) can
;; be used to map a function to the tab key
;; see (equal (kbd "TAB") (kbd "C-i")) -> t
;; see (equal (kbd "<tab>") (kbd "C-i")) -> nil
(define-key global-map (kbd "<tab>") #'indent-for-tab-command)

;; make crtl g act as escape
(declare-function evil-normal-state "evil.el")
(define-key evil-insert-state-map (kbd "C-g") #'evil-normal-state)
(define-key evil-replace-state-map (kbd "C-g") #'evil-normal-state)

(declare-function evil-delete-backward-char-and-join "evil.el")
(define-key evil-insert-state-map (kbd "C-h") #'evil-delete-backward-char-and-join)
(define-key evil-replace-state-map (kbd "C-h") #'evil-delete-backward-char-and-join)

(define-key evil-insert-state-map (kbd "M-h") #'backward-kill-word)
(define-key evil-replace-state-map (kbd "M-h") #'backward-kill-word)

(declare-function evil-set-initial-state "evil.el")
(evil-set-initial-state 'messages-buffer-mode 'normal)
(evil-set-initial-state 'dired-mode 'normal)
(evil-set-initial-state 'eshell-mode 'insert)

;; some keybindings for the command window
(defvar evil-ex-completion-map)
(define-key evil-ex-completion-map (kbd "M-p") #'previous-complete-history-element)
(define-key evil-ex-completion-map (kbd "M-n") #'next-complete-history-element)

;; personal key map
(defun increment-number-at-point (n)
  "Increment the number at POINT with value of N."
  (interactive "p")
  (skip-chars-backward "0-9")
  (skip-chars-backward "-")
  (unless (looking-at "-?[0-9]+")
    (error "No number at point"))
  (replace-match
   (number-to-string
    (+ n (string-to-number (match-string 0))))))
(define-key evil-normal-state-map (kbd "<SPC> p i n") #'increment-number-at-point)

(defun decrement-number-at-point (n)
  "Dencrement the number at POINT with value of N."
  (interactive "p")
  (increment-number-at-point (- n)))
(define-key evil-normal-state-map (kbd "<SPC> p d n") #'decrement-number-at-point)

(defun move-line-up (n)
  "Move current line up by N."
  (interactive "p")
  (forward-line 1)
  (transpose-lines (- n))
  (forward-line -1))
(define-key evil-normal-state-map (kbd "<SPC> p m l u") #'move-line-up)

(defun move-line-down (n)
  "Move current line down by N."
  (interactive "p")
  (forward-line 1)
  (transpose-lines n)
  (forward-line -1))
(define-key evil-normal-state-map (kbd "<SPC> p m l d") #'move-line-down)

;; TODO use keymap-set instead of define-key
;; TODO when building emacs add the source dir to /usr/share and let the
;; configuration know about this until NEXT
(defun evil-check-previous-and-pass (command-list alternative)
  "Return a functional callback.

Run the continuation unless the previous command is in COMMAND-LIST, in that
case run ALTERNATIVE."
  (lambda (cont)
    (lambda ()
      (interactive)
      (if (member last-command command-list)
	  (funcall-interactively alternative)
	(funcall-interactively cont)))))


(defun define-key-continuation (keymap key def)
  "Similar with `define-key'.

Instead, DEF is a continuation which takes as its input the previous command.
KEY and KEYMAP are as in `define-key'."
  (define-key keymap key (funcall def (lookup-key keymap key))))

(define-key-continuation
 evil-normal-state-map (kbd "q")
 (evil-check-previous-and-pass
  '(eldoc-doc-buffer)
  #'evil-quit-documentation))

  ;; TODO how do I do this?
(defun evil-quit-documentation ()
  "Quit the eldoc buffer after `eldoc-doc-buffer'."
  ;; I want to be able to quit the documentation with q if documentation
  ;; was previously opened
  (quit-window nil (get-buffer-window "*eldoc*")))
;; NEXT

(with-eval-after-load 'evil
  (straight-use-package 'evil-collection)
  (declare-function evil-collection-init "evil-collection.el")
  (defvar evil-collection-mode-list)
  (with-eval-after-load 'evil-collection
    (setq evil-collection-mode-list (remove 'lispy evil-collection-mode-list)))
  (evil-collection-init)

  ;; provide a version of vimtutor for emacs
  (straight-use-package 'evil-tutor)

  ;; surround.vim
  (straight-use-package 'evil-surround)
  (declare-function global-evil-surround-mode "evil-surround.el")
  (global-evil-surround-mode 1))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--evil)
;;; --evil.el ends here
