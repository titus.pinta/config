;;;; --git.el --- Configuration for version control systems -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; Use magit as a the main git porcelain and set up useful git adjacent features
;; like time machine and fringe markings.

;;; Code:
(declare-function straight-use-package "straight.el")
(straight-use-package 'magit)

(with-eval-after-load 'magit
  (when (featurep '--evil)
    (declare-function evil-define-key* "evil-core.el")
    (defvar magit-diff-mode-map)
    (defvar diff-mode-map)
    (evil-define-key* 'normal magit-diff-mode-map
      (kbd "] ]") 'diff-hunk-next
      (kbd "[ [") 'diff-hunk-prev)
    (evil-define-key* 'normal diff-mode-map
      (kbd "] ]") 'diff-hunk-next
      (kbd "[ [") 'diff-hunk-prev))

  (defvar smerge-command-prefix)
  (setq smerge-command-prefix (kbd "C-c s")))

;; gutter is a git display on the fringe (near line numbers)
(straight-use-package 'git-gutter)
(straight-use-package 'git-gutter-fringe)

(with-eval-after-load 'git-gutter
  (require 'git-gutter-fringe)
  (defvar git-gutter:update-interval)
  (setq git-gutter:update-interval 0.02)

  ;; make git gutter take less space
  (define-fringe-bitmap
    'git-gutter-fr:added [224] nil nil '(center repeated))
  (define-fringe-bitmap
    'git-gutter-fr:modified [224] nil nil '(center repeated))
  (define-fringe-bitmap
    'git-gutter-fr:deleted [224] nil nil '(center repeated)))

(declare-function global-git-gutter-mode "git-gutter")
(global-git-gutter-mode 1)

;; view previous git versions of file
(straight-use-package 'git-timemachine)
(with-eval-after-load 'git-timemachine
  (set-face-attribute 'git-timemachine-minibuffer-author-face nil
		      :foreground 'unspecified
		      :weight 'normal
		      :inherit 'font-lock-keyword-face)
  (set-face-attribute 'git-timemachine-minibuffer-detail-face nil
		      :foreground 'unspecified
		      :inherit 'font-lock-warning-face))

;; git auto push
(defvar init.el-autogit-projects '()
  "List of projects to for git auto commit.")

(defun init.el-autogit-add-projects ()
  "Add current p0roject to list to recieve automated commits."
  (push (project-root (project-current t)) init.el-autogit-projects))

(declare-function project-root "project.el")
(defun init.el-autogit-auto-commit ()
  "Commit current file to git if it is in `init.el-autogit-projects'."
  (make-thread
   (lambda ()
     (when (member (project-root (project-current))
		   init.el-autogit-projects)
       (shell-command (format  "git add %s" (buffer-file-name)))
       (shell-command (format  "git commit -m 'autocommit at %s'"
			       (format-time-string "%Y-%m-%d %H:%M:%S"))))))
  nil)

(add-hook 'write-file-functions #'init.el-autogit-auto-commit)

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--git)
;;; --git.el ends here
