;;;; --tabbar.el --- Configuration for the tabbar -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:

;;; Code:
(require 'tab-bar)
(tab-bar-mode 1)

(setq tab-bar-new-tab-choice "*scratch*")

;; hide the ugly buttons
(setq-default tab-bar-close-button-show nil
      tab-bar-new-button-show nil)

(declare-function straight-use-package "straight.el")
(straight-use-package '(tab-bar-display :type git :host github
				       :repo "ROCKTAKEY/tab-bar-display"))
;; (require 'tab-bar-display)

;; globalized version of a mode is active in all buffers
(declare-function tab-bar-display-mode "tab-bar-display.el")
(defvar tab-bar-display-mode)
(autoload #'tab-bar-display-mode "tab-bar-display.el")
(define-globalized-minor-mode global-tab-bar-display-mode
  tab-bar-display-mode
  (lambda () (tab-bar-display-mode 1))
  :group 'tab-bar-display)
(global-tab-bar-display-mode 1)

(require 'subr-x)
(defun --utils/length-tab-bar ()
  "Compute the total length of the strings displayed in the tab bar.

This sums  up the number of characters in the names of all the tabs."
  (length (string-join
	   (mapcar (lambda (item) (alist-get 'name (cdr item))) (tab-bar-tabs))
	   " ")))

;; TODO change all the icons (when the bug is fixed)
;; TODO add color
(defun --utils/srepr-network-connection ()
  "Prettify the name of the network interface, using icons."
  (let ((connection (active-network-interface)))
    (cond
     ((string-match "\\<w" connection) (format "%s  " (wifi-ssid)))
     ((string-match "\\<e" connection) "Wired ")
     ((string-match "" connection) "No connection")
     (t (error "Error understanding the network interface (ip link)")))))

(defun --utils/srepr-battery ()
  "Prettify the battery percentage, using icons."
  (let ((battery-perc (battery-perc)))
    (format "%d%%%% %s " battery-perc
	    (cond
	     ((string-match "\\<Charging" (battery-status)) "")
	     ((> battery-perc 75) "")
	     ((> battery-perc 50) "")
	     ((> battery-perc 25) "")
	     ((> battery-perc 5) "")
	     (t "")))))

;; TODO have a nice block based system
(defun --utils/tab-bar-string ()
  "Construct the info string to be display in the tab bar."
  (string-join
   (list
    (--utils/srepr-battery)
    (--utils/srepr-network-connection)
    (format "%.0f%%%%  " (cpu-perc)) ;this gets formatted twice, %%%% makes %
    (format "%.1fGHz  "(/ (cpu-freq) 1073741824.0)) ;1073741824 = 1024^3 = 2^30
    (format "%s  "(file-size-human-readable (memory-used)))
    (format-time-string "%a %e %b  %R")
    ) "")
  )

;; TODO status blocks
(defvar tab-bar-display-after)
(setq tab-bar-display-after
      (when (eq system-type 'gnu/linux)
	'(:eval
	  (format
	   (format "%%%ds " (- (frame-width)
			       (--utils/length-tab-bar) 3))
	   (--utils/tab-bar-string)))))

(run-with-timer 1 t #'force-mode-line-update)

;; Customization for using gnome
(when (string-match-p "\\(gnome\\)\\|\\(ubuntu\\)"
		      (format "%s" (getenv "DESKTOP_SESSION")))
  (global-tab-bar-display-mode -1))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--tabbar)
;;; --tabbar.el ends here
