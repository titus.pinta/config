;;;; --desktop.el --- Configuration for emacs as a desktop environment  -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; Set up basic desktop environment tools, like video player mail reader and
;; rss and password store

;;; Code:
(declare-function straight-use-package "straight.el")

(straight-use-package '(empv :host github :repo "isamert/empv.el"))
(require 'empv)
(setq empv-invidious-instance "https://iv.melmac.space/api/v1")

(provide '--desktop)
;;; --desktop.el ends here
