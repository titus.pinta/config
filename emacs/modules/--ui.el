;;;; --ui.el --- Configuration for the basic UI features  -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; Set up basic ui elements, required by most modes like line numbers and
;; highlights. Also improve the help experience, using a better help mode and
;; which key.

;;; Code:
(require 'display-line-numbers)
(setq display-line-numbers-type 'relative)
(setq display-line-numbers-current-absolute t)

(setq-default display-fill-column-indicator-column 80)

(declare-function straight-use-package "straight.el")

(straight-use-package 'rainbow-mode)
(declare-function rainbow-mode "rainbow-mode.el")

(straight-use-package 'rainbow-delimiters)
(declare-function rainbow-delimiters-mode "rainbow-delimiters.el")

(straight-use-package 'highlight-numbers)
(declare-function highlight-numbers-mode "highlight-numbers.el")

(straight-use-package 'highlight-escape-sequences)
(defvar hes-mode-alist)
(declare-function hes-mode "highlight-escape-sequences.el")

;; highlight escape sequences
;; Do i need hes in other modes? or just in prog mode
;; hes mode seems top not work
;; todo write this from scratch using font lock
(with-eval-after-load 'hes-mode
  (add-to-list 'hes-mode-alist
	       '(python-mode . ,hes-c/c++/objc-escape-sequence-re)))
(hes-mode 1)

;; activate required behaviour for text-editing/programming
(defun init.el/lambdas/--activate-ui-elements ()
  "Activate all the basic ui features for a better editing experience."
  (interactive)
  (setq show-trailing-whitespace t)
  (display-line-numbers-mode 1)
  (display-fill-column-indicator-mode 1)
  (rainbow-mode 1)
  (rainbow-delimiters-mode 1)
  (highlight-numbers-mode 1))

(add-hook 'text-mode-hook #'init.el/lambdas/--activate-ui-elements)
(add-hook 'prog-mode-hook #'init.el/lambdas/--activate-ui-elements)
(add-hook 'git-timemachine-mode-hook #'init.el/lambdas/--activate-ui-elements)


;; help configuration
;; this is a more complex version of the built in help
;; it does not replace the default help, because it is slower
;; it should be used when extra info is needed (like who calls
;; a given function)
(setq help-window-select t)

(straight-use-package 'helpful)
(declare-function helpful "helpful.el")

(straight-use-package 'elisp-demos)
(declare-function elisp-demos-advice-helpful-update "elisp-demos.el")
(declare-function elisp-demos-advice-describe-function-1 "elisp-demos.el")
(advice-add 'helpful-update :after #'elisp-demos-advice-helpful-update)
(advice-add 'describe-function-1 :after #'elisp-demos-advice-describe-function-1)

(straight-use-package 'which-key)
(defvar which-key-idle-delay)
(declare-function which-key-mode "which-key.el")
(setq which-key-idle-delay 1.0)
(which-key-mode 1)

;; the build in info system synergieses well with the help mode and can be
;; augmented with a significant quantity of external documentation
(add-to-list 'Info-default-directory-list "~/.local/share/info")

;; enable highlighter mode
;; WIP until this gets on elpa
(load-file "~/Projects/software/highlighter/highlighter.el" )
(declare-function highlighter-mode "highlighter.el")
(highlighter-mode)

;; when in the scratch buffer, or any other lisp interaction buffer
;; C-x f should open the recent files
(declare-function consult-recent-file "consult.el")
(add-hook 'lisp-interaction-mode-hook
	  (lambda () (define-key (current-local-map) (kbd "C-x f")
			    #'consult-recent-file)))

;; (straight-use-package 'beacon)
;; (declare-function beacon-mode "beacon.el")
;; (beacon-mode 1)

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--ui)
;;; --ui.el ends here
