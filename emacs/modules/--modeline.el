;;;; --modeline.el --- Configure the modeline -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; Set up 'doom-modeline using 'anzu to emulate the vim search behavior.

;;; Code:
(declare-function straight-use-package nil)
(straight-use-package 'doom-modeline)

(column-number-mode 1)

(with-eval-after-load 'doom-modeline
  (defvar doom-modeline-height)
  (defvar doom-modeline-minor-modes)
  (defvar doom-modeline-buffer-encoding)
  (defvar doom-modeline-bar-width)
  (setq doom-modeline-height 0
	doom-modeline-minor-modes 1
	doom-modeline-buffer-encoding 1
	doom-modeline-bar-width 4)

  (straight-use-package '(nerd-icons :type git :host github
				     :repo "rainstormstudio/nerd-icons.el"))
  (require 'nerd-icons))

(declare-function doom-modeline-mode "domm-modeline.el")
(doom-modeline-mode 1)

;; anzu shows the number of matches in the mode line
(straight-use-package 'anzu)
(with-eval-after-load 'anzu
  ;; use a variable watcher to get the vim behavior (color red if no matches)
  (add-variable-watcher
   'anzu--total-matched
   (lambda (_ new __ ___)
     (when (numberp new)
       (if (/= new 0)
	   (set-face-attribute 'doom-modeline-panel nil
			       :inherit 'modus-themes-active-blue)
	 (set-face-attribute 'doom-modeline-panel nil
			     :inherit 'error)))))

  (straight-use-package 'evil-anzu)
  (require 'evil-anzu))

(declare-function global-anzu-mode "anzu-mode.el")
(global-anzu-mode 1)

;; minions removes minor modes from the line
(straight-use-package 'minions)
(declare-function minions-mode "minions.el")
(minions-mode 1)

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--modeline)
;;; --modeline.el ends here
