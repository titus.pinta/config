;;;; --email.el --- Configuration mu4e -*- lexical-binding: t -*-

;; Copyright (C) 2025 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:

;;; Code:



(provide '--email)
;;; --email.el ends here
