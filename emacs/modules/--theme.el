;;;; --theme.el --- Configure the theme and fonts  -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; The theme is 'modus-vivendi, unless the desktop environment is Gnome, in
;; which case Adwaita is used. Fira code is the default  font with ligatures
;; enabled.
;; Font lock configuration, that does not fit in any other module is also here.
;; The 'select-font-family function and its derivatives provide visual selection
;; and manipulation of fonts.

;;; Code:
;; use the build in modus themes
;; some options are not yet in the build in modus theme
;; investigate when this will be enabled and add them TODO
;; modus theme file
(declare-function straight-use-package "straight.el")
(require 'modus-themes)

(setq modus-themes-italic-constructs t
      modus-themes-bold-constructs t
      ;; modus-themes-mixed-fonts nil ;TODO
      modus-themes-variable-pitch-ui nil
      ;; modus-themes-custom-auto-reload t ;TODO
      ;; modus-themes-disable-other-themes t ;TODO
      modus-themes-org-blocks 'gray-background)

(if (string-match-p "\\(gnome\\)\\|\\(ubuntu\\)"
		    (format "%s" (getenv "DESKTOP_SESSION")))
    (progn
      (set-face-attribute 'default nil :height 135)
      (straight-use-package 'adwaita-dark-theme)
      (require 'adwaita-dark-theme)
      (load-theme 'adwaita-dark t))
  (load-theme 'modus-vivendi t))

;; define basic colors to be used in modes emulating a (alacritty-style) terminal

(defvar 8-colors/white "#ffffff")
(defvar 8-colors/black "#000000")
(defvar 8-colors/red "#ff5f59")
(defvar 8-colors/green "#44bc44")
(defvar 8-colors/yellow "#d0bc00")
(defvar 8-colors/blue "#2fafff")
(defvar 8-colors/magenta "#feacd0")
(defvar 8-colors/cyan "#00d3d0")


;; find the local name for the fira code font
(defvar --fira-code-font
  (car-safe (seq-filter
	     (lambda (font)
	       (and
		(string-match-p "Fira" font)
		(string-match-p "Code" font)))
	     (font-family-list)))
  "Name for the Fira Code font.")

;; Font config
(set-face-attribute 'default nil :family --fira-code-font :height 100)
(when (string-match-p "\\(gnome\\)\\|\\(ubuntu\\)"
		      (format "%s" (getenv "DESKTOP_SESSION")))
  (set-face-attribute 'default nil :height 130))

(set-face-attribute 'fixed-pitch nil :family "Fira Code")
(set-face-attribute 'italic nil :family "Fira Code")
(set-face-attribute 'italic nil :underline 'unspecified)

(set-face-attribute 'font-lock-comment-face nil :inherit 'italic)

(straight-use-package 'ligature)
;; This produces a compile time warning that that appears in Emacs,
;; so the require has too be wrapped and the functions in the file need to
;; be declared.
;; Maybe it requires a ligature table file that is not available at compile time
;; There is no error.
(ignore-errors (require 'ligature))
(declare-function ligature-set-ligatures "ligature.el")
(ligature-set-ligatures
 't '(;; == === ==== => =| =>>=>=|=>==>> ==< =/=//=// =~
      ;; =:= =!=
      ("=" (rx (+ (or ">" "<" "|" "/" "~" ":" "!" "="))))
      ;; ;; ;;;
      (";" (rx (+ ";")))
      ;; && &&&
      ("&" (rx (+ "&")))
      ;; !! !!! !. !: !!. != !== !~
      ("!" (rx (+ (or "=" "!" "\." ":" "~"))))
      ;; ?? ??? ?:  ?=  ?.
      ("?" (rx (or ":" "=" "\." (+ "?"))))
      ;; %% %%%
      ("%" (rx (+ "%")))
      ;; |> ||> |||> ||||> |] |} || ||| |-> ||-||
      ;; |->>-||-<<-| |- |== ||=||
      ;; |==>>==<<==<=>==//==/=!==:===>
      ("|" (rx (+ (or ">" "<" "|" "/" ":" "!" "}" "\]"
                      "-" "=" ))))
      ;; \\ \\\ \/
      ;; \n \t
      ("\\" (rx (or "/" (+ "\\") "n" "t")))
      ;; ++ +++ ++++ +>
      ("+" (rx (or ">" (+ "+"))))
      ;; :: ::: :::: :> :< := :// ::=
      (":" (rx (or ">" "<" "=" "//" ":=" (+ ":"))))
      ;; // /// //// /\ /* /> /===:===!=//===>>==>==/
      ("/" (rx (+ (or ">"  "<" "|" "/" "\\" "\*" ":" "!"
                      "="))))
      ;; .. ... .... .= .- .? ..= ..<
      ("\." (rx (or "=" "-" "\?" "\.=" "\.<" (+ "\."))))
      ;; -- --- ---- -~ -> ->> -| -|->-->>->--<<-|
      ("-" (rx (+ (or ">" "<" "|" "~" "-"))))
      ;; *> */ *)  ** *** ****
      ("*" (rx (or ">" "/" ")" (+ "*"))))
      ;; www wwww
      ("w" (rx (+ "w")))
      ;; <> <!-- <|> <: <~ <~> <~~ <+ <* <$ </  <+> <*>
      ;; <$> </> <|  <||  <||| <|||| <- <-| <-<<-|-> <->>
      ;; <<-> <= <=> <<==<<==>=|=>==/==//=!==:=>
      ;; << <<< <<<<
      ("<" (rx (+ (or "\+" "\*" "\$" "<" ">" ":" "~"  "!"
                      "-"  "/" "|" "="))))
      ;; >: >- >>- >--|-> >>-|-> >= >== >>== >=|=:=>>
      ;; >> >>> >>>>
      (">" (rx (+ (or ">" "<" "|" "/" ":" "=" "-"))))
      ;; #: #= #! #( #? #[ #{ #_ #_( ## ### #####
      ("#" (rx (or ":" "=" "!" "(" "\?" "\[" "{" "_(" "_"
                   (+ "#"))))
      ;; ~~ ~~~ ~=  ~-  ~@ ~> ~~>
      ("~" (rx (or ">" "=" "-" "@" "~>" (+ "~"))))
      ;; __ ___ ____ _|_ __|____|_
      ("_" (rx (+ (or "_" "|"))))
      ;; Fira code: 0xFF 0x12
      ("0" (rx (and "x" (+ (in "A-F" "a-f" "0-9")))))
      ;; Fira code:
      "Fl"  "Tl"  "fi"  "fj"  "fl"  "ft"
      ;; The few not covered by the regexps.
      "{|"  "[|"  "]#"  "(*"  "}#"  "$>"  "^="))
(declare-function global-ligature-mode "ligature.el")
(global-ligature-mode 1)

(global-prettify-symbols-mode 1)
;; (setq prettify-symbols-unprettify-at-point 'right-edge)

;; highlight todos ;;this is a todo
(add-hook 'prog-mode-hook
	  (lambda () (font-lock-add-keywords
		 nil '(("TODO" 0 'font-lock-warning-face t)))))

;; Font selection
(defun select-font-family ()
  "Select a font family from all available fonts."
  (interactive)
  (let ((--list (font-family-list))
	(completion-extra-properties
	 '(:annotation-function
	   (lambda (--family)
	     (let ((--str "The quick brown fox jumps over the lazy dog."))
	       (concat
		(format (format "%%%ds" (- 40 (string-width --family))) " ")
		(propertize
		 --str 'face
		 `(:inherit
		   ,(if (featurep 'marginalia) 'marginalia-documentation
		      'comlpetions-annotations)
		   :family ,--family))))))))
     (completing-read "Font Family: " --list nil t)))

(defun yank-font-family ()
  "Select a font family from all available fonts and yank it to the kill ring."
  (interactive)
  (kill-new (call-interactively #'select-font-family)))

(defun set-default-font-family ()
  "Select a font family from all available fonts and set it as the default."
  (interactive)
  (let ((--family (call-interactively #'select-font-family) ))
    (set-face-attribute 'default nil :family --family)
    (set-face-attribute 'fixed-pitch nil :family --family)
    (set-face-attribute 'italic nil :family --family)))

(defun set-default-font-height (height)
  "Prompt for the new HEIGHT of the default font."
  (interactive
   (list (read-number (format "Font height (current %d): "
			      (face-attribute 'default :height)))))
  (set-face-attribute 'default nil :height height))

;; Download missing fonts
(unless
    (file-regular-p "~/.local/share/fonts/FiraCode-RegularItalic.otf")
  (url-copy-file
   "https://github.com/Avi-D-coder/FiraCode-italic/raw/master/FiraCode-RegularItalic.otf"
   "~/.local/share/fonts/FiraCode-RegularItalic.otf"))
(unless
    (file-regular-p "~/.local/share/fonts/FiraCode-BoldItalic.otf")
  (url-copy-file
   "https://github.com/Avi-D-coder/FiraCode-italic/raw/master/FiraCode-BoldItalic.otf"
   "~/.local/share/fonts/FiraCode-BoldItalic.otf"))

;; Highlight lines too long
(defvar toggle-highlight-lines-too-long nil
  "Represent the highlighting of lines over 80 characters.")
(defun toggle-highlight-lines-too-long ()
  "Toggle the highlighting of lines over 80 characters."
  (interactive)
  (if toggle-highlight-lines-too-long
      (progn
	(setq toggle-highlight-lines-too-long nil)
	(unhighlight-regexp t)
	(message "Removing highlights from lines over 80 characters"))
    (setq toggle-highlight-lines-too-long t)
    (highlight-lines-matching-regexp ".\\{81\\}" 'error)
    (message "Add highlights to lines over 80 characters")))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--theme)
;;; --theme.el ends here
