;;;; --org.el --- Configuration for org-mode -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; Set up for org mode.

;; TODO: figure out how to compute the scale for latex preview
					; maybe at compile time

;;; Code:
(declare-function straight-use-package "straight.el")

(straight-use-package 'org)
(with-eval-after-load 'org
  (declare-function only-in-debug nil)
  (only-in-debug (message "Debug: defer loading: org loaded"))

  (defvar org--inhibit-version-check nil
    "Inhibit version check for org.")

  ;; * Visual elements
  (straight-use-package 'org-bullets)

  (defvar org-bullets-bullet-list)
  (setq org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●"))
  (declare-function org-bullets-mode "org-bullets")
  (add-hook 'org-mode-hook #'org-bullets-mode)

  ;; to better mimic wysiwyg editors, org should wrap lines, not truncate them
  (add-hook 'org-mode-hook (lambda () (toggle-truncate-lines -1)))

  (defvar org-hide-emphasis-markers)
  (defvar org-ellipsis)
  (setq org-hide-emphasis-markers nil
	org-ellipsis "▾")


  ;; * Latex preview
  (defvar org-startup-with-inline-images)
  (setq org-startup-with-inline-images t)
  (defvar org-format-latex-options)
  (setq org-format-latex-options
	(plist-put org-format-latex-options :scale 1.5))
  (declare-function org-latex-preview "org")
  (add-hook 'org-mode-hook (lambda () (org-latex-preview '(16))))

  (defvar org-highlight-latex-and-related)
  (setq org-highlight-latex-and-related '(native))

  ;; * Org latex style keybindings
  (defvar org-mode-map)
  (define-key org-mode-map (kbd "$")
	      (lambda ()
		(interactive)
		(if (= (char-before) ?$)
		    (progn
		      (delete-char -1)
		      (insert "\\(\\)")
		      (backward-char)
		      (backward-char))
		  (call-interactively #'self-insert-command))))

  ;; * Babel languages
  (straight-use-package 'mermaid-mode)
  (straight-use-package 'ob-mermaid)

  (straight-use-package 'ess)

  (defvar org-confirm-babel-evaluate nil)
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((julia . t)
     (haskell . t)
     (C . t)
     (mermaid . t)
     (scheme . t)
     (emacs-lisp . t)))

  ;; * Agenda configuration
  ;; data files for agenda and calendar
  (customize-set-value 'org-agenda-files '("~/.local/share/Agenda/Agenda.org"))
  (customize-set-value 'org-agenda-diary-file "~/.local/share/Agenda/Agenda.org")
  (customize-set-value 'org-agenda-include-diary t)
  (customize-set-value 'diary-file "~/.local/share/Agenda/diary")
  (customize-set-value 'calendar-date-style 'european)

  (customize-set-value 'calendar-holidays
		       '((holiday-fixed 1 1 "Neujahr")
			 (holiday-easter-etc -2 "Karfreitag")
			 (holiday-easter-etc 1 "Ostermontag")
			 (holiday-fixed 5 1 "Tag der Arbeit")
			 (holiday-easter-etc 39 "Christi Himmelfahrt")
			 (holiday-easter-etc 50 "Pfingstmontag")
			 (holiday-fixed 10 3 "Tag der Deutschen Einheit")
			 (holiday-fixed 12 25 "1. Weihnachtstag")
			 (holiday-fixed 12 26 "2. Weihnachtstag")
			 (holiday-fixed 10 31 "Reformationstag")))

  ;; open agenda file
  (defvar org-agenda-files)
  (defun org-agenda-find-file ()
    "Open org agenda files."
    (interactive)
    (cond
     ((eq 0 (length org-agenda-files)) (error "No org agenda file"))
     ((eq 1 (length org-agenda-files)) (find-file (car org-agenda-files)))
     (t (find-file
	 (completing-read "Find agenda file:" org-agenda-files nil t)))))

  ;; calendar week start
  (customize-set-value 'calendar-week-start-day 1)

  ;; highlight in the calendar days that have org agenda entries
  ;; TODO modernize this
  (declare-function org-agenda-get-day-entries "org-agenda")
  (declare-function calendar-mark-visible-date "calendar")
  (declare-function calendar-day-of-week "calendar")
  ;; an advice to mark all required dates after `calendar-generate-month'
  (advice-add
   'calendar-generate-month :after
   (lambda (month year _)
     ;; loop over the days of the month
     (make-thread
      (lambda ()
	(dotimes (i 31)
	  ;; get the entries from the agenda for DATE
	  (let* ((date (list month (1+ i) year))
		 (entries (org-agenda-get-day-entries
			   (car org-agenda-files) date)))
	    ;; count the number of entries with travel tags
	    (if (>= (length
		     (seq-filter (lambda (s) (string-match-p ":travel:" s)) entries))
		    1)
		(calendar-mark-visible-date date 'error)
	      ;; test whether the day needs to be highlighted
	      (if (>= (length entries) 1)
		  (calendar-mark-visible-date date 'warning)
		;; check if date is weekend
		(when (or (= (calendar-day-of-week date) 6)
			  (= (calendar-day-of-week date) 0))
		  (calendar-mark-visible-date
		   date 'font-lock-comment-face))))))))))

  (advice-add
   'calendar :before
   (lambda (&optional _) (require 'org-agenda)))
  ;; (advice-remove 'calendar-generate-month nil)
  ;; Here

  ;; Mark today in calendar
  (add-hook 'calendar-today-visible-hook 'calendar-mark-today)
  (set-face-attribute 'calendar-today nil :underline 'unspecified)
  (set-face-attribute 'calendar-today nil :inherit 'highlight)


  ;; * Use latex in org mode
  (declare-function LaTeX-math-mode "latex.el")
  (add-hook 'org-mode-hook #'LaTeX-math-mode)
  (defvar tex--prettify-symbols-alist)
  (add-hook 'org-mode-hook
	    (lambda ()
	      (setq prettify-symbols-alist
		    (append prettify-symbols-alist
			    tex--prettify-symbols-alist))))

  ;; Hide math delimiters
  (font-lock-add-keywords
   'org-mode
   '(("\\$" 0 'font-lock-comment-face)))

  ;; * Note taking with roam
  (straight-use-package 'org-roam)
  (with-eval-after-load 'org-roam
    (only-in-debug (message "Debug: defer loading: org-roam loaded"))
    (straight-use-package 'f)
    (declare-function f-dir-p "f")
    (unless (f-dir-p "~/Notes/Roam") (mkdir "~/Notes/Roam" t))

    (eval-when-compile (defvar org-roam-directory))
    (setq org-roam-directory "~/Notes/Roam")

    (eval-when-compile (defvar org-roam-completion-everywhere))
    (setq org-roam-completion-everywhere t)

    (declare-function org-roam-db-autosync-enable "org-roam.el")
    (org-roam-db-autosync-enable)

    (defvar org-roam-mode-map)
    (declare-function org-roam-node-insert "org-roam.el")
    (define-key org-mode-map (kbd "C-c i") #'org-roam-node-insert)

    (straight-use-package '(org-roam-ui :type git :host github
					:repo "org-roam/org-roam-ui"))))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--org)
;;; --org.el ends here
