;;;; --dired.el --- Configuration for dired -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; Configure the built in package Dired.
;; It uses font lock to transform the build in Dired into a modern interface
;; using exa style colored chmod permissions and file name coloring depending on
;; the file type

;;; Code:
(add-hook 'dired-mode-hook #'display-line-numbers-mode)
;; update dired after any file change
(add-hook 'dired-mode-hook #'auto-revert-mode)
(setq dired-listing-switches "-alhF --group-directories-first")
(require 'dired)

;; open child directories in the same buffer
(declare-function straight-use-package "straight.el")

(straight-use-package 'dired-single)
(declare-function dired-single-buffer "dired-single.el")

(when (featurep '--evil)
  (declare-function evil-collection-define-key "evil-collection.el")
  (evil-collection-define-key 'normal 'dired-mode-map
    "h" 'dired-single-up-directory
    "l" 'dired-single-buffer
    "m" 'dired-mark
    "d" 'dired-flag-file-deletion
    "s" 'dired-find-file-other-window
    "u" 'dired-unmark))

;; this uses any other dired buffer as the target directory for copy/move
(setq dired-dwim-target 1)

;; add icons
(straight-use-package '(nerd-icons-dired
			:type git :host github
			:repo "rainstormstudio/nerd-icons-dired"))

(declare-function nerd-icons-dired-mode "nerd-icons-dired.el")
(add-hook 'dired-mode-hook #'nerd-icons-dired-mode)

;; use trash instead of rm
(setq delete-by-moving-to-trash t)
;; dired does not update itself fast enough after trash
(advice-add 'dired-internal-do-deletions :after #'revert-buffer)

;; face definition for dired
;; define faces for the chmod permission string
(defface dired-face-chmod-font-lock-dir
  `((t :inherit ,(if (featurep 'marginalia) 'marginalia-file-priv-dir
		   'font-lock-keyword-face)))
  "Face for chmod directory bits in Dired buffers."
  :group 'dired)
(defface dired-face-chmod-font-lock-read
  `((t :inherit ,(if (featurep 'marginalia) 'marginalia-file-priv-read
		   'font-lock-type-face)))
  "Face for chmod readable bits in Dired buffers."
  :group 'dired)
(defface dired-face-chmod-font-lock-write
  `((t :inherit ,(if (featurep 'marginalia) 'marginalia-file-priv-write
		   'font-lock-builtin-face)))
  "Face for chmod writable bits in Dired buffers."
  :group 'dired)
(defface dired-face-chmod-font-lock-exec
  `((t :inherit ,(if (featurep 'marginalia) 'marginalia-file-priv-exec
		   'font-lock-function-name-face)))
  "Face for chmod executable bits in Dired buffers."
  :group 'dired)
(defface dired-face-chmod-font-lock-dir-bold
  '((t :inherit font-lock-keyword-face :weight bold))
  "Bold face for chmod directory bits in Dired buffers."
  :group 'dired)
(defface dired-face-chmod-font-lock-read-bold
  '((t :inherit dired-face-chmod-font-lock-read  :weight bold))
  "Bold face for chmod readable bits in Dired buffers."
  :group 'dired)
(defface dired-face-chmod-font-lock-write-bold
  '((t :inherit dired-face-chmod-font-lock-write :weight bold))
  "Bold face for chmod writable bits in Dired buffers."
  :group 'dired)
(defface dired-face-chmod-font-lock-exec-bold
  '((t :inherit dired-face-chmod-font-lock-exec :weight bold))
  "Bold face for chmod executable bits in Dired buffers."
  :group 'dired)
(defface dired-face-info-font-lock-user
  '((t :inherit dired-face-chmod-font-lock-dir :weight normal))
  "Face for user name in Dired buffers."
  :group 'dired)
(defface dired-face-info-font-lock-size
  '((t :inherit dired-face-chmod-font-lock-write :weight normal))
  "Face for size in Dired buffers."
  :group 'dired)
(defface dired-face-info-font-lock-time
  '((t :inherit dired-face-chmod-font-lock-read :weight normal))
  "Face for time in Dired buffers."
  :group 'dired)
;; define faces for file types in dired
(defface dired-face-elisp
  '((t :inherit font-lock-builtin-face :weight normal))
  "Face for elisp files."
  :group 'dired)
(defface dired-face-latex
  '((t :foreground "cyan"))
  "Face for LaTeX files."
  :group 'dired)
(defface dired-face-pdf
  '((t :foreground "red"))
  "Face for pdf files."
  :group 'dired)
(defface dired-face-docx
  '((t :foreground "blue"))
  "Face for docx files."
  :group 'dired)

;; highlight the chmod permission strings
(defconst dired--info-regexp
  "\\([dl-]\\)\\([r-]\\)\\([w-]\\)\\([x-]\\)\\([r-]\\)\\([w-]\\)\\([x-]\\)\\([r-]\\)\\([w-]\\)\\([xsgt-]\\) *[0-9]* *\\([^ ]*\\) *[^ ]* *\\([^ ]*\\) *\\([^ ]* *[^ ]* *[^ ]*\\)"
  "Regex to match the chmod permission sting in Dired mode.")

;; TODO use only one regexp here
(font-lock-add-keywords
 'dired-mode
 `((,dired--info-regexp 1 'dired-face-chmod-font-lock-dir-bold t)
   (,dired--info-regexp 2 'dired-face-chmod-font-lock-read-bold t)
   (,dired--info-regexp 3 'dired-face-chmod-font-lock-write-bold t)
   (,dired--info-regexp 4 'dired-face-chmod-font-lock-exec-bold t)
   (,dired--info-regexp 5 'dired-face-chmod-font-lock-read t)
   (,dired--info-regexp 6 'dired-face-chmod-font-lock-write t)
   (,dired--info-regexp 7 'dired-face-chmod-font-lock-exec t)
   (,dired--info-regexp 8 'dired-face-chmod-font-lock-read t)
   (,dired--info-regexp 9 'dired-face-chmod-font-lock-write t)
   (,dired--info-regexp 10 'dired-face-chmod-font-lock-exec t)
   (,dired--info-regexp 11 'dired-face-info-font-lock-user t)
   (,dired--info-regexp 12 'dired-face-info-font-lock-size t)
   (,dired--info-regexp 13 'dired-face-info-font-lock-time t)))

;; highlight all unimportant info in dired up to the filename (size and date)
;; with the comment font
(font-lock-add-keywords
 'dired-mode `((,dired-move-to-filename-regexp  0 'font-lock-comment-face)))

;; highlight files based on file extension
;; TODO
(defun dired--make-regexp-from-extension (ext)
  "Generate regexp that matches file names with extension EXT."
  (concat dired-move-to-filename-regexp "\\(.*" ext "\\)$"))

(font-lock-add-keywords
 'dired-mode
 `(("[^ ]*\\.el\\>"  0 'dired-face-elisp t) ; TODO deal with this
   (".*~"  0 'font-lock-comment-face)))

;; change directory after mkdir
(advice-add 'mkdir :after
	    (lambda (dir &optional _)
	      (when (eq major-mode 'dired-mode)
		(dired-single-buffer dir))))


;; TODO this does not yet work
(with-eval-after-load 'dired)
;; dired save point
(defvar dired--point-alist ())
(defun dired--save-point ()
  (let ((--saved-point (alist-get default-directory dired--point-alist
				  nil nil #'string-equal)))
    (unless (and --saved-point (= (point) --saved-point))
      (setf (alist-get default-directory dired--point-alist
		       nil nil #'string-equal)
	    (point)))))

(add-hook 'dired-mode-hook
	  (lambda () (add-hook 'post-command-hook #'dired--save-point :local)))

(add-variable-watcher
 'default-directory
 (lambda (_ newval __ ___)
   (let ((--saved-point (alist-get newval
				   dired--point-alist
				   nil nil #'string-equal)))
     (when (and (eq 'dired-mode major-mode) --saved-point)
       (goto-char --saved-point)))))

(defun dired-yank-relative-path ()
  "Yank the path of the file under point, relative to the file in other window.

This is used when trying to import a file in a different source file. One can
open the included file's folder's Dired in the next window and navigate to
the file and then call this function."
  (interactive nil dired-mode)
  (kill-new
   (file-relative-name
    (dired-get-filename)
    (file-name-directory (buffer-file-name (window-buffer (next-window)))))))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--dired)
;;; --dired.el ends here
