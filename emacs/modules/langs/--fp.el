;;;; --fp.el --- Configuration for functional programming -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config.git
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:

;;; Code:
(declare-function straight-use-package "straight.el")
(straight-use-package 'tuareg)
(straight-use-package 'haskell-mode)


;; module Main where
;; import Prelude

;; baby_func :: Int -> Int
;; baby_func x = x + 3

;; my_add :: Int -> Int -> Int
;; my_add x y = x + y

;; -- pattern matching
;; fact :: Int -> Int
;; fact 0 = 1
;; fact n = n * fact (n - 1)

;; data MyNat = MyZero | MySucc MyNat

;; to_nat :: Int -> MyNat
;; to_nat 0 = MyZero
;; to_nat x = MySucc (to_nat (x - 1))

;; -- destructuring in patterns
;; print_nat :: MyNat -> Int
;; print_nat MyZero = 0
;; print_nat (MySucc n) = 1 + (print_nat n)

;; -- lists
;; data MyList t = E | t :-> (MyList t)
;; infixr :->

;; my_head :: MyList t -> t
;; my_head (x :-> xs) = x

;; my_tail :: MyList t -> MyList t
;; my_tail (x :-> xs) = xs

;; my_map :: (a -> b) -> (MyList a) -> (MyList b)
;; my_map _ E = E
;; my_map f (x :-> xs) = (f x) :-> (my_map f xs)
;; my_map_example = my_map (1 + ) (1 :-> 10 :-> 23 :-> E)


;; -- streams
;; -- codata types
;; data S t = S
;;   {  h :: t
;;   ,  t :: (S t)
;;   }

;; all_from :: Int -> (S Int)
;; all_from n = S h t where
;;   h = n
;;   t = all_from (n + 1)

;; ia :: Int -> (S t) -> [t]
;; ia 0 _ = []
;; ia n s = (h s) : (ia (n - 1) (t s))


;; -- lazy evaluation
;; fibo = 1 : 1 : zipWith (+) fibo (tail fibo)

;; primes = sieve [2..] where
;;   sieve (p : xs) = p : sieve [x | x <- xs, x `mod` p /= 0]

;; main = putStrLn "Hello world!"


;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--fp)
;;; --fp.el ends here
