;;;; --julia.el --- Cofiguration for julia development -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; TODO autostart eglot using ensure
;; TODO Clear this up

;;; Code:
;; julia needs latex mode for the prefix that should be used in order to expand
;; latex commands into unicode
(declare-function straight-use-package "straight.el")
(straight-use-package 'julia-mode)


(defvar julia-snail-repl-buffer)
(with-eval-after-load 'julia-mode

  ;; find julia executable
  (setq-default
   julia-snail-executable
   (if (or (file-regular-p "/usr/bin/julia") (file-regular-p "/bin/julia"))
       "julia"
     (string-trim (shell-command-to-string
		   "zsh -c \"source $HOME/.zshrc && where julia\""))))

  ;; use corfu's autocomplete
  (add-hook 'julia-mode-hook
	    (lambda () (setq-local corfu-auto t)))

  ;; use tree sitter
  (declare-function tree-sitter-mode "tree-sitter.el")
  (add-hook 'julia-mode-hook #'tree-sitter-mode)

  (straight-use-package 'julia-snail)
  (require 'julia-snail)

  (declare-function julia-snail-mode "julia-snail.el")
  (add-hook 'julia-mode-hook #'julia-snail-mode)
  (straight-use-package 'eglot-jl)
  (declare-function eglot-jl-init "eglot-jl.el")
  (eglot-jl-init)

  ;; start the eglot server using a new thread
  (declare-function eglot "eglot.el")
  (add-hook 'julia-mode-hook #'eglot-ensure)

  (defvar julia-snail-port)
  (defvar julia-snail-port-next julia-snail-port
    "Lowest port number free for a julia snail repl.")

  (defvar julia-snail-repl-buffer-just-created '()
    "List of julia snail repl buffer that are currently created.")

  ;; have snail use the current project if it exists
  (advice-add 'julia-snail :before
	      (lambda ()
		;; set the number of threads for julia to auto
		(unless (get-buffer julia-snail-repl-buffer)
		  (setenv "JULIA_NUM_THREADS" "auto"))
		;; set the buffer name and port
		(setq julia-snail-port julia-snail-port-next)
		(setq julia-snail-port-next (1+ julia-snail-port-next))
		(if (project-current)
		    (progn
		      (setq julia-snail-repl-buffer
			    (format "*julia %s*" (project-name (project-current))))
		      ;; mark the buffer if this is the first time snail is ran
		      (unless (get-buffer julia-snail-repl-buffer)
			(push julia-snail-repl-buffer
			      julia-snail-repl-buffer-just-created)))
		  (setq julia-snail-repl-buffer "*julia*"))))
  ;; use a new thread for the firs run of julia snail TODO
  ;; (if (get-buffer julia-snail-repl-buffer)
  ;; 	  (julia-snail)
  ;; 	(make-thread #'julia-snail))))

  (require 'project)
  (declare-function project-current "project.el")
  (declare-function project-root "project.el")
  (declare-function project-name "project.el")
  (declare-function julia-snail--send-to-repl "julia-snail.el")
  (advice-add 'julia-snail :after
	      (lambda ()
		;; when this is the first time julia snail is called in a buffer
		(when (member julia-snail-repl-buffer
			      julia-snail-repl-buffer-just-created)
		  (setq julia-snail-repl-buffer-just-created
			(delq julia-snail-repl-buffer
			      julia-snail-repl-buffer-just-created))
		  (when (and
			 (project-current)
			 (file-exists-p (concat
					 (project-root (project-current))
					 "Project.toml")))
		    (julia-snail--send-to-repl
		     (format "cd(\"%s\")"
			     (expand-file-name (project-root (project-current))
					       ))
		     :async nil)
		    (julia-snail--send-to-repl "using Revise" :async nil)
		    (julia-snail--send-to-repl "Pkg.activate(\".\")" :async nil)
		    (julia-snail--send-to-repl
		     (format "using %s" (project-name (project-current)))
		     :async nil)))))

  ;; overwrite the lisp eval key binding with the julia eval
  (defvar julia-snail-mode-map)
  (declare-function julia-snail-send-dwim "julia-snail.el")
  (define-key julia-snail-mode-map [remap eval-last-sexp] #'julia-snail-send-dwim)
  (define-key julia-snail-mode-map (kbd "C-c p C-x C-e") #'eval-last-sexp)

  ;; (advice-remove 'julia-snail nil)

  ;; ignore snail capf when there is no repl
  (advice-add 'julia-snail-repl-completion-at-point :around
	      (lambda (f &optional module-finder)
		(when (get-buffer julia-snail-repl-buffer)
		  (apply f module-finder)))))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--julia)
;;; --julia.el ends here
