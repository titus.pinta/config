;;;; --web.el --- Configuration for html, css and js tools -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; Set up editing html css and markdown using emmet.
;;; Code:
;; markdown mode
(declare-function straight-use-package "straight.el")
(straight-use-package 'markdown-mode)

(defun expand-markdown--html ()
  "Use pandoc to generate html from markdown style syntax.

This works when region is active, replacing it."
  (interactive)
  (when mark-active
    (shell-command-on-region (mark) (point)
			     "pandoc -f markdown -t latex"
			     nil t)))
(add-hook 'html-mode-hook (lambda () (defalias 'expand-markdown
				  (symbol-function 'expand-markdown--html))))

;; * Live Preview
(straight-use-package 'impatient-mode)
(straight-use-package 'simple-httpd)
(declare-function httpd-start "simple-httpd.el")
(with-eval-after-load 'sgml-mode
  (httpd-start))
(declare-function impatient-mode "impatient-mode.el")
(add-hook 'html-mode-hook #'impatient-mode)
(add-hook 'css-mode-hook #'impatient-mode)
(add-hook 'js-mode-hook #'impatient-mode)


;; emmet
(straight-use-package '(emmet-for-emacs :type git :host github
					:repo "joostkremers/emmet-for-emacs"))

;; download the two missing required files for emmet-for-emacs
(declare-function f-file-p "f")
(declare-function f-dir-p "f")
(declare-function f-dirname "f")
(dolist (json '("snippets.json" "preferences.json"))
  (let ((f (concat (straight--build-dir "emmet-for-emacs/conf") json)))
    (unless (f-file-p f)
      (unless (f-dir-p (f-dirname f)) (mkdir (f-dirname f)))
      (url-copy-file
       (concat
	"https://raw.githubusercontent.com/joostkremers/emmet-for-emacs/master/conf/"
	json)
       f))))


(declare-function emmet-mode "emmet.el")
(add-hook 'sgml-mode-hook #'emmet-mode)
(add-hook 'css-mode-hook #'emmet-mode)

(with-eval-after-load 'emmet
  ;; use tab completion
  (defvar emmet-mode)
  (declare-function emmet-expand-line "emmet.el")
  (advice-add 'indent-for-tab-command
	      :after (lambda (&optional _)
		       (when mark-active (expand-markdown--html))
		       (when emmet-mode (emmet-expand-line nil)))
	      '((name . "expand-emmet-and-markdown-with-tab")))

  ;; TODO
  ;; remove the key bindings
  (defvar emmet-mode-keymap)
  (define-key emmet-mode-keymap (kbd "C-j") nil)
  (define-key emmet-mode-keymap (kbd "<C-return>") nil)
  (define-key emmet-mode-keymap (kbd "<C-M-right>") nil)
  (define-key emmet-mode-keymap (kbd "<C-M-left>") nil)
  (define-key emmet-mode-keymap (kbd "C-c C-c w") nil))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--web)
;;; --web.el ends here
