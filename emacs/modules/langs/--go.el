;;;; --go.el --- Configuration for Go projects -*- lexical-binding: t -*-

;; Copyright (C) 2025 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for licensing info

;;; Commentary:

;;; Code:

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(declare-function straight-use-package "straight.el")
(straight-use-package 'go-ts-mode)

(add-to-list 'auto-mode-alist '("\\.go\\'" . go-ts-mode))
(add-hook 'go-ts-mode-hook #'eglot-ensure)

(defvar project-run-command)
(add-hook 'go-ts-mode-hook
	  (lambda () (setq project-run-command
		      (lambda () "" "go run $(find . | grep main.go)"))))


(provide '--go)
;;; --go.el ends here
