;;;; --elisp.el --- Configuration for editing Emacs lisp files -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; This file provides functionality that makes editing elisp files more
;; convenient, together with configuration for the flymake binding for elisp.

;;; Code:

;; editing defaults for elisp
(add-hook 'lisp-mode-hook (lambda () (indent-tabs-mode -1)))
;; the evil behavior of C-j (newline) is better
(define-key lisp-interaction-mode-map (kbd "C-j") nil)

;; insert header info
(defun project-insert-header (descr)
  "Insert the project specific information in the header.

The information that is inserted in the beginning includes the filename,the
description DESCR, the copyright notice, the license identifier, the url, and
the comment section headings.

The footer includes the provides statement and the end of file notice."
  (interactive "sDescription: ")
  (let ((--marker (point-marker)))
    (goto-char (point-min))
    (insert (format ";;;; %s --- %s -*- lexical-binding: t -*-\n\n"
		    (file-relative-name (buffer-file-name)) descr)
	    ";; Copyright (C) 2023 Titus Pinta\n"
	    ";; SPDX-License-Identifier: GPL-3.0-or-later\n"
	    (let ((--project-url (shell-command-to-string
				  "git remote show origin")))
	      (if (string-match "Fetch URL: \\(.*\\)" --project-url)
		  (format ";; Part of %s\n" (match-string 1 --project-url))
		""))
	    ";; See LICENSE.md for the license\n"
	    ";; See end of file for more info\n\n"
	    ";;; Commentary:\n\n"
	    ";;; Code:\n")
    (goto-char (point-max)) ;;todo the warranty depends on the license
    (insert ";; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.\n\n"
	    (format "(provide '%s)\n" (file-name-base (buffer-file-name)))
	    (format ";;; %s ends here\n"
		    (file-relative-name (buffer-file-name))))
    (goto-char (marker-position --marker))))


;; configure the flymake byte compiler to use the same packages as Emacs
(setq elisp-flymake-byte-compile-load-path load-path)
(add-to-list 'elisp-flymake-byte-compile-load-path "./")

;; make #'add-to-list 'load-path add to 'elisp-flymake-byte-compile-load-path
(advice-add 'add-to-list :after
	    (lambda (list-var element &optional append compare-fn)
	      (when (eq list-var 'load-path)
		(add-to-list 'elisp-flymake-byte-compile-load-path
			     element append compare-fn))))

(declare-function straight-use-package "straight.el")
(straight-use-package 'lispy)
(declare-function lispy-mode "lispy.el")
(autoload 'lispy-mode "lispy.el")
(straight-use-package 'lispyville)
(declare-function lispiville-mode "lispyville.el")

(declare-function lispy-mode "lispy.el")
(declare-function lispyville-mode "lispyville.el")
(add-hook 'lisp-mode-hook #'lispy-mode)
(add-hook 'emacs-lisp-mode-hook #'lispy-mode)
(add-hook 'lisp-interaction-mode-hook #'lispy-mode)
(add-hook 'lispy-mode-hook #'lispyville-mode)

;; overwrite the annoying bindings from lispy to maintain the evil behaviour
;; [ in lispy mode this moves forward to the next sexpr
;; TODO (caddr lispy-mode-map) is hard coded. Find a way to replace it
(with-eval-after-load 'lispy
  (defvar lispy-mode-map)
  (define-key (caddr lispy-mode-map) (kbd "[") nil)
  (define-key lispy-mode-map (kbd "[") nil)
  ;; (define-key (caddr lispy-mode-map) (kbd "{") nil)
  ;; (define-key lispy-mode-map (kbd "{") nil)
  ;; (define-key (caddr lispy-mode-map) (kbd "}") nil)
  ;; (define-key lispy-mode-map (kbd "}") nil)
  (define-key (caddr lispy-mode-map) (kbd "]") nil)
  (define-key lispy-mode-map (kbd "]") nil))

;; TODO this has an error
(with-eval-after-load 'lispyville
  (defvar lispyville-mode-map)
  (define-key lispyville-mode-map [remap evil-yank-line]
    (kmacro '[?y ?y] 0 "%d")))


(declare-function lispy--insert-or-call "lispy.el")
(declare-function lispy--out-forward "lispy.el")
(declare-function evil-normal-state "evil.el")
(declare-function evil-set-jump "evil.el")

(defun lispyville-exit ()
  "Exit sexpr or enter normal mode."
  (interactive)
  (let ((--p (point))
	(--f (lispy--insert-or-call
	      #'evil-normal-state
	      '(:inserter (lambda () (interactive)
			    (unless (lispy--out-forward 1)
			      (evil-normal-state)))))))
    ;; if inside a string this is weird TODO
    (funcall --f)
    ;; if the point doesn't move, try to move again and exit to normal state
    (when (= --p (point))
      (lispy--out-forward 1)
      (evil-normal-state))
    ;; if the point moved, set the jump
    (unless (= --p (point)) (evil-set-jump --p))))

(when (featurep '--evil)
  (defvar evil-replace-state-local-map)
  (defvar evil-insert-state-local-map)
  (declare-function lispy-delete-backward "lispy.el")
  (define-key evil-replace-state-local-map (kbd "C-h") #'lispy-delete-backward)
  (define-key evil-insert-state-local-map (kbd "C-h") #'lispy-delete-backward)
  (define-key evil-replace-state-local-map (kbd "<escape>") #'lispyville-exit)
  (define-key evil-insert-state-local-map (kbd "<escape>") #'lispyville-exit))

;; dash is a better functional programming environment for emacs
(straight-use-package 'dash)
;; (require 'dash)


;; ide features for elips
(straight-use-package 'erefactor)


;; useful features for using Emacs to develop Emacs code
(defun restart-buffer (&optional eval)
  "Kill the current buffer, revisit the file and evaluate if EVAL is non-nil.

This is used to reset all the buffer local variables when developing new
functions for Emacs editing, for instance when prototyping new font lock
key words."
  (interactive "P")
  (let ((--file (buffer-file-name)))
    (when --file
      (kill-buffer (current-buffer))
      (find-file --file)
      (when eval (eval-buffer)))))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--elisp)
;;; --elisp.el ends here
