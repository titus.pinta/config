;;;; --latex.el --- Configuration for writing latex -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; Use auctex for better latex editing. Integrate latex mode with evil.

;;; Code:
(declare-function straight-use-package "straight.el")
(straight-use-package 'pdf-tools)

(with-eval-after-load 'doc-view
  (require 'pdf-tools))

(declare-function pdf-view-midnight-minor-mode "pdf-tools.el")
(add-hook 'pdf-view-mode-hook #'pdf-view-midnight-minor-mode)

;; * PDF Tools
(with-eval-after-load 'pdf-tools
  ;; TODO message only in debug mode
  (message "PDF_TOOLS")
  (require 'pdf-occur)
  (require 'pdf-history)
  (require 'pdf-links)
  (require 'pdf-outline)
  ;; (require 'pdf-annot) 			; this blocks pdf-tools-install
  (require 'pdf-sync)
  (declare-function pdf-tools-install "pdf-tools.el")
  (pdf-tools-install)
  (declare-function doc-view-toggle-display "doc-view.el")
  (add-hook 'doc-view-mode-hook #'doc-view-toggle-display)
  (defvar pdf-view-midnight-colors)
  (setq pdf-view-midnight-colors
	`(,(face-attribute 'default :foreground) .
	  ,(face-attribute 'default :background))))

(straight-use-package 'auctex)

(with-eval-after-load 'latex
  ;; setup modern tex style
  (advice-add 'TeX-style-list :override
	      (lambda (&rest _) '("latex2e")))

  ;; install pdf-tools
  (require 'pdf-tools)

  ;; activate related tex mods
  ;; *** Reference mode
  (add-hook 'LaTeX-mode-hook #'reftex-mode)
  ;; *** Outline mode
  (add-hook 'LaTeX-mode-hook #'outline-minor-mode)
  ;; *** Math mode for useful math macros
  (declare-function LaTeX-math-mode "latex.el")
  (add-hook 'LaTeX-mode-hook #'LaTeX-math-mode)
  ;; *** Math fold mode
  (declare-function TeX-fold-mode "tex-fold.el")
  (add-hook 'LaTeX-mode-hook #'TeX-fold-mode)
  ;; *** Flyspell mode
  (add-hook 'LaTeX-mode-hook #'flyspell-mode)
  (add-hook 'LaTeX-mode-hook #'flymake-mode)
  ;; *** Evil take mode  for vim like key bindings in tex
  (straight-use-package 'evil-tex)
  (declare-function evil-tex-mode "evil-tex.el")
  (add-hook 'LaTeX-mode-hook #'evil-tex-mode)


  ;; use consult to complete references and citations
  (straight-use-package 'consult-tex)
  (require 'consult-tex)

  ;; use $ to exit from the math environment
  (defvar LaTeX-mode-map)
  (define-key LaTeX-mode-map (kbd "$")
	      (lambda ()
		(interactive)
		(if (and (= (char-after) ?$) (not (= (char-before) ?$)))
		    (forward-char)
		  (call-interactively #'self-insert-command))))

  (defvar LaTeX-electric-left-right-brace)
  (setq LaTeX-electric-left-right-brace t)

  (defvar font-latex-fontify-sectioning)
  (setq font-latex-fontify-sectioning 'color)


  ;; integration of project with latex
  (define-key LaTeX-mode-map (kbd "C-x p c") 'TeX-command-run-all)

  ;; * Tempo mod
  ;; the regex for tempo to know where a template begins
  (require 'tempo)
  (setq-default tempo-match-finder "\\(<.*\\)\\=")
  (defvar tempo-interactive)
  (setq tempo-interactive t)

  (declare-function tempo-define-template "tempo.el")
  (tempo-define-template "begin-equation"
			 '("\\begin{equation}" > n>
			   r> n>
			   "\\end{equation}" >)
			 "<eq"
			 "Insert a LaTeX equation.")

  (tempo-define-template "begin-align"
			 '("\\begin{align}" > n>
			   r> n>
			   "\\end{align}" >)
			 "<al"
			 "Insert a LaTeX align.")

  (tempo-define-template "begin-theorem"
			 '("\\begin{theorem}" > n>
			   r> n>
			   "\\end{theorem}" >)
			 "<thm"
			 "Insert a LaTeX theorem.")

  (tempo-define-template "begin-definition"
			 '("\\begin{definition}" > n>
			   r> n>
			   "\\end{definition}" >)
			 "<def"
			 "Insert a LaTeX definition.")

  (tempo-define-template "begin-proof"
			 '("\\begin{proof}" > n>
			   r> n>
			   "\\end{proof}" >)
			 "<proof"
			 "Insert a LaTeX proof.")

  (tempo-define-template "begin-proposition"
			 '("\\begin{proposition}" > n>
			   r> n>
			   "\\end{proposition}" >)
			 "<prop"
			 "Insert a LaTeX proposition.")

  (tempo-define-template "begin-environment"
			 '("\\begin{" (p "Environment: " environment) "}" > n>
			   r> n>
			   "\\end{" (s environment) "}" >)
			 "<env"
			 "Insert a LaTeX environment.")
  ;; (with-eval-after-load 'LaTeX
  ;;   (define-abbrev latex-mode-abbrev-table "<eq" "" 'tempo-template-begin-equation))

  (defun LaTeX-expand-env ()
    "Expand <env into environment."
    (interactive)
    (save-excursion
      (move-beginning-of-line nil)
      ))
  ;; <env
  ;; Only do this in LatexMode
  ;; add a dictionary to translate
  ;; expand templates whit completion
  (declare-function tempo-expand-if-complete "tempo.el")
  (advice-add 'indent-for-tab-command
	      :after (lambda (&optional _)
		       (when mark-active (expand-markdown--latex))
		       (tempo-expand-if-complete))
	      '((name . "expand-tempo-and-markdown-with-tab")))

  ;; useful editing keybindings
  ;; underlines and overlines
  (defvar LaTeX-math-keymap)
  (declare-function LaTeX-math-overline "tex.el")
  (define-key LaTeX-math-keymap (kbd "_ o") #'LaTeX-math-overline)
  (declare-function LaTeX-math-underline "tex.el")
  (define-key LaTeX-math-keymap (kbd "_ u") #'LaTeX-math-underline)

  ;; fraction
  (declare-function LaTeX-math-frac "tex.el")
  (define-key LaTeX-math-keymap (kbd "_ u") #'LaTeX-math-underline)

  ;; sequence commands
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "0") (lambda () (interactive)
			      (insert "x^0")))
    (define-key m (kbd "k") (lambda () (interactive)
			      (insert "x^k")))
    (define-key m (kbd "p") (lambda () (interactive)
			      (insert "x^{k + 1}")))
    (define-key m (kbd "x") (lambda () (interactive)
			      (insert "x^{}")
			      (backward-char)))
    (define-key m (kbd "y") (lambda () (interactive)
			      (insert "x^{}")
			      (backward-char)))
    (define-key LaTeX-math-keymap (kbd "6") m))

  ;; blackboard bold keymap
  ;;  this is inspired by the mathcal keymap `LaTeX-math-cal'
  (defun LaTeX-math-bb (char)
    "Insert a \\mathbb{CHAR}. Inspired by `LaTeX-math-cal'."
    (interactive "*c")
    (if (member char '(?R ?M ?N ?Z ?C))
	(insert "\\bb" (char-to-string char))
      (insert "\\mathbb{" (char-to-string char) "}")))
  (define-key LaTeX-math-keymap (kbd "B") 'LaTeX-math-bb)

  (defun LaTeX-math-bar (char)
    "Insert a \\CHARbar. Inspired by `LaTeX-math-cal'."
    (interactive "*c")
    (insert "\\" (char-to-string char) "bar"))

  ;; keybinding for partial
  (declare-function LaTeX-math-partial "latex.el")
  (define-key LaTeX-math-keymap (kbd "'") 'LaTeX-math-partial)

  ;; use varepsilon instead of epsilon
  (declare-function LaTeX-math-varepsilon "latex.el")
  (advice-add 'LaTeX-math-epsilon :override #'LaTeX-math-varepsilon)

  ;; acutex overwrites some style preferences, so they have to be re enabled
  ;; in the style hook
  (add-hook 'TeX-update-style-hook
	    (lambda () (font-lock-add-keywords
		   nil '(("TODO" 0 'font-lock-warning-face t)))))

  ;; TODO find a better way to do this
  ;; modify spell check regexp to match cref in order to ignore it
  (setq-default
   flyspell-tex-command-regexp
   "\\(\\(begin\\|end\\)[ 	]*{\\|\\(cite[a-z*]*\\|label\\|ref\\|cref\\|eqref\\|usepackage\\|documentclass\\)[ 	]*\\(\\[[^]]*\\]\\)?{[^{}]*\\)")
  ;; modify font lock regexp to match cref
  (setq-default
   font-latex-match-reference
   "\\\\\\(\\(?:\\(?:Ref\\|bibliography\\|c\\(?:haptermark\\|ite\\)\\|eqref\\|cref\\|foot\\(?:note\\(?:mark\\|text\\)?\\|ref\\)\\|glossary\\|in\\(?:clude\\|dex\\|put\\)\\|label\\|marginpar\\|nocite\\|pa\\(?:geref\\|ragraphmark\\)\\|ref\\|s\\(?:\\(?:ection\\|ub\\(?:paragraph\\|s\\(?:\\(?:ubs\\)?ection\\)\\)\\)mark\\)\\|vref\\)\\)\\>\\)")

  ;; make latex look like math
  (defvar tex--prettify-symbols-alist)
  (with-eval-after-load 'tex-mode
    (dolist
	(--symbol '(("\\xbar" . (?x (tl . cl) ?̄))
		    ("\\ybar" . (?y (tl . cl) ?̄))
		    ("\\tbar" . (?t (tl . cl) ?̄))
		    ("\\Hbar" . (?H (tl . cl) ?̄))
		    ("\\bbR" . ?ℝ)
		    ("\\bbN" . ?ℕ)
		    ("\\bbC" . ?ℂ)
		    ("\\bbM" . ?𝕄)
		    ("\\bbP" . ?ℙ)
		    ("\\bbE" . ?𝔼)
		    ("\\bbH" . ?ℍ)
		    ("\\bbRn" . (?ℝ (tr . cl) ?n))
		    ("\\bbRm" . (?ℝ (tr . cl) ?m))
		    ("\\bbRnxn" . (?ℝ (tr . cl) ?n (tr . tl) ?× (tr . tl) ?n))
		    ("\\bbRnxm" . (?ℝ (tr . cl) ?n (tr . tl) ?× (tr . tl) ?m))
		    ("\\bbRmxm" . (?ℝ (tr . cl) ?m (tr . tl) ?× (tr . tl) ?m))
		    ("\\bbRmxn" . (?ℝ (tr . cl) ?m (tr . tl) ?× (tr . tl) ?n))
		    ("\\setto" .  ?⇉)
		    ("\\dots" . ?…)
		    ("\\tilde{f}" . (?f (tl . cl) ?˜))
		    ("\\tilde{F}" . (?F (tl . cl) ?˜))
		    ("\\tilde{H}" . (?H (tl . cl) ?˜))
		    ("\\diff" . ?𝚍)
		    ("\\lparan{}" . 40)
		    ("\\rparan{}" . 41)
		    ("\\left" . ?𑗔)
		    ("\\right" . ?𑗔)
		    ("\\{" . 123)
		    ("\\}" . 125)
		    ("\\mathcalalt{e}" . ?𝓮)
		    ))
      (push --symbol tex--prettify-symbols-alist)))

  ;; use display \overline{} using font overlines

  (add-hook 'LaTeX-mode-hook
	    (lambda () (add-to-list 'font-lock-extra-managed-props 'display)))

  (let ((--overline-re "\\(\\\\overline{\\)\\(.*\\)\\(}\\)"))
    (font-lock-add-keywords
     'LaTeX-mode
     `((,--overline-re 2 '(face nil :overline t) append)
       (,--overline-re 1 '(face nil display ""))
       (,--overline-re 3 '(face nil display "")))))

  ;; C-c C-f is already mapped to something in LaTeX mode, so
  ;; 'consult-recent-file needs to be mapped to it again. needs a hook because
  ;; of auctex and variable not found
  (declare-function consult-recent-file "consult.el")
  (define-key LaTeX-mode-map (kbd "C-c C-f") #'consult-recent-file)

  (defvar TeX-view-program-selection)
  (defvar TeX-source-correlate-start-server)
  (setq TeX-view-program-selection '((output-pdf "PDF Tools"))
	TeX-source-correlate-start-server t)

  ;; 'pdf-view-revert moves the point after the content of the pdf file \[M-f]
  ;; this code moves the point back in the pdf file after reverting
  ;; to see the problem use 'forward-word in a pdf document
  ;; TODO this still does not work well
  ;; This is a complete hack and important TODO
  (declare-function TeX-active-master "tex.el")
  (declare-function TeX-output-extension "tex.el")
  (advice-add 'TeX-command-master
	      :after (lambda (&optional _)
		       (run-with-timer
			1 nil
			(lambda ()
			  (when (bufferp (TeX-active-master
					  (TeX-output-extension)))
			    (dolist (win (get-buffer-window-list
					  (TeX-active-master
					   (TeX-output-extension))
					  nil t))
			      (set-window-point win 1))))))
	      '((name . "set-cursor-correctly-after-revert-pdf-buffers")))

  ;; delimiters
  ;; (require 'smartparens)
  ;; (sp-local-pair 'tex-mode "$" "$")
  ;; (require 'wrap-region)
  ;; (add-hook 'LaTeX-mode-hook #'wrap-region-mode)
  ;; (wrap-region-add-wrapper "$" "$" nil 'tex-mode)

  ;; some ligatures have problems with the way tex is displayed in emacs
  (declare-function ligature-set-ligatures "ligature.el")
  (ligature-set-ligatures
   'LaTeX-mode '("www" "**" "***" "**/" "*>" "*/" "\\\\" "\\\\\\" "::"
		 ":::" ":=" "!!" "!=" "!==" "-}" "----" "-->" "->" "->>"
		 "-<" "-<<" "-~" "#{" "#[" "##" "###" "####" "#(" "#?" "#_"
		 "#_(" ".-" ".=" ".." "..<" "..." "?=" "??" ";;" "/*" "/**"
		 "/=" "/==" "/>" "//" "///" "&&" "||" "||=" "|=" "|>" "^=" "$>"
		 "++" "+++" "+>" "=:=" "==" "===" "==>" "=>" "=>>" "<="
		 "=<<" "=/=" ">-" ">=" ">=>" ">>" ">>-" ">>=" ">>>" "<*"
		 "<*>" "<|" "<|>" "<$" "<$>" "<!--" "<-" "<--" "<->" "<+"
		 "<+>" "<=" "<==" "<=>" "<=<" "<>" "<<" "<<-" "<<=" "<<<"
		 "<~" "<~~" "</" "</>" "~@" "~-" "~>" "~~" "~~>" "%%"))

  ;; SyncTeX
  (declare-function TeX-source-correlate-mode "tex.el")
  (add-hook 'LaTeX-mode-hook #'TeX-source-correlate-mode)

  ;; use modern features like eglot
  (add-hook 'LaTeX-mode-hook #'eglot-ensure)
  )

;; Outline mode
(add-hook 'LaTeX-mode-hook #'outline-minor-mode)

;; * Usefull macros

(defun expand-markdown--latex ()
  "Use pandoc to generate latex from markdown style syntax.

This works when region is active, replacing it."
  (interactive)
  (when mark-active
    (shell-command-on-region (mark) (point)
			     "pandoc -f markdown -t latex"
			     nil t)))
(add-hook 'LaTeX-mode-hook (lambda () (defalias 'expand-markdown
				  (symbol-function 'expand-markdown--latex))))


;; Fraction capf
(defun capf-latex-fractions ()
  "Expand common fraction notation into latex."
  (interactive)
  (message "Compliting")
  (save-excursion
    (backward-word 2)
    (when (looking-at (rx (group (+ num)) (* whitespace) ?/ (group (+ num))))
      (list (match-beginning 0)
	    (match-end 0)
	    (list (format "%s" (match-string 0)))
	    :exclusive 'no
	    :exit-function
	    (lambda (s _)
	      (delete-region (match-beginning 0) (match-end 0))
	      (insert (let ((parts (split-string s "/")))
			(format "\\frac{%s}{%s}" (car parts) (cadr parts)))))
	    :annotation-function
	    (lambda (s) (let ((parts (split-string s "/")))
		     (format "\\frac{%s}{%s}" (car parts) (cadr parts))))))))

;; Tools to handle bibliography files
(defun to-bib ()
  "Convert the current buffer from .ris to .bib."
  (interactive)
  (when mark-active
    (shell-command-on-region (mark) (point)
			     "ris2xml | xml2bib"
			     nil t)))
;; TODO make this function better. To deal with edge cases.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide '--latex)
;;; --latex.el ends here
