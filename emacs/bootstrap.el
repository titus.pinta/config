;;;; bootstrap.el --- Use straight.el to set up the load path -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Part of https://gitlab.com/titus.pinta/config/
;; See LICENSE.md for the license
;; See end of file for more info

;;; Commentary:
;; Sets up straight.el and some of the special load paths and sets the variables
;; that are required in early init of other modules, like 'evil-want-keybinding.

;;; Code:
(require 'cl-lib) ;; use cl-lib instead of cl

;; macro to insert code only if building for debugging
(defvar debug-build (getenv "DEBUG")
  "Controls weather the init.elc file should have the debugging instruments.")
(eval-when-compile
  (if (and (boundp 'debug-build) debug-build)
      (defmacro only-in-debug (body)
	body)
    (defmacro only-in-debug (_) ())))

;; hide warnings in windows
(defvar warning-minimum-level)
(when (eq system-type 'windows-nt)
  (setq warning-minimum-level :emergency))

;; place for startup debugging scripts
(only-in-debug (message "
==============================
       -DEBUG INIT.EL-
==============================
"))
(only-in-debug (toggle-debug-on-error))

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el"
			 user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(require 'straight)
(when (and (boundp 'init.el-compiling)
	   init.el-compiling
	   (getenv "STRAIGHT_INIT"))
  (ignore-errors (straight-pull-recipe-repositories)))

;; benchmark init
(only-in-debug
 (progn
   (straight-use-package 'benchmark-init)
   (require 'benchmark-init)
   (benchmark-init/activate)))

;; Org is required for lispy to work properly (I don't know why)
(straight-use-package 'org)
(straight-use-package 'pdf-tools)
(require 'org)

;; vertico extensions
(add-to-list
 'load-path
 (expand-file-name "straight/build/vertico/extensions" straight-base-dir))

;; modus theme file
(add-to-list 'load-path (expand-file-name "themes" data-directory))


;; disable key bindings because it conflicts with evil collections
;; this has to be here in order to avoid byte compiler warnings when loading
;; 'evil-collection
(defvar evil-want-keybinding)
(setq evil-want-keybinding nil)

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(provide 'bootstrap)
;;; bootstrap.el ends here
