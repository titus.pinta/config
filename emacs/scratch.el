;;;; scratch.el --- Scratch file for emacs configuration -*- lexical-binding: t -*-

;; Copyright (C) 2023 Titus Pinta
;; SPDX-License-Identifier: GPLv3
;; Part of https://gitlab.com/titus.pinta/config.git
;; See LICENSE.md for licensing info

;;; Commentary:

;;; Code:

;; Do not forget about <tab> and TAB
;; see (equal (kbd "TAB") (kbd "C-i")) -> t
;; see (equal (kbd "<tab>") (kbd "C-i")) -> nil

;; golden ratio
;; (straight-use-package 'golden-ratio)
;; (require 'golden-ratio)
(golden-ratio-mode 1)
(setq golden-ratio-max-width 100)

;; prettify symbols

;; Multi threading support
(defun foo ()
  (sleep-for 3)
  (message (format "%d\n" (count-lines (point-max) (point-min))))
  )
(foo)

(make-thread #'foo)
(make-thread #'flyspell-buffer)


(advice-remove 'pdf-view-redisplay nil)

(push '("\\xbar" . 0x0078) prettify-symbols-alist)

;; multi cursor
(straight-use-package 'multiple-cursors)

;; consult preview at point
(add-hook 'minibuffer-setup-hook #'consult-preview-at-point-mode)

;; power management
(transient-define-prefix power-manager ()
  ["Power managment"
   [
    ("s" "suspend" (lambda () (interactive) (shell-command "systemctl suspend")))
    ("p" "power off" (lambda () (interactive) (shell-command "systemctl poweroff")))
    ]]
  )
;; make splits horizontal by default
;; (setq split-width-threshold 72)

(advice-add 'dired-internal-do-deletions :after #'revert-buffer) ;; this is not necessary

;; using the built in emacs trashing system is better than using trash-cli
;; use trash-cli to remove files
(defun system-move-file-to-trash (filename)
  (shell-command-to-string (concat "trash '" filename "'")))

(straight-use-package 'evil-multiedit)


;; this makes opening a file to slow, use 'make-thread
;; (add-hook 'flyspell-mode-hook #'flyspell-buffer)
;; (add-hook 'flyspell-prog-mode-hook #'flyspell-buffer)

;; TODO set the x root
;; (run-with-timer 1 1 (lambda () (shell-command-to-string (format (format "xsetroot -name \"%s\"" (--utils/tab-bar-string))))))

(add-to-list 'elisp-flymake-byte-compile-load-path "/usr/share/emacs/28.2/etc/themes/")


;; deal with opening vterm in lower term
(split-window (frame-root-window) (- (window-total-height) 10) 'below)

(setq display-buffer-alist nil)
(add-to-list 'display-buffer-alist
	     '("\\*julia Thesis\\*" (display-buffer-in-side-window)))
(buffer-list)


;; git timemachine
(straight-use-package 'git-timemachine)
(require 'git-timemachine)
(add-hook 'git-timemachine-mode-hook #'--config/lambda/--activate-ui-elements)

(advice-add 'julia-snail-repl-completion-at-point :around
	    (lambda (f &optional module-finder)
	      (when (get-buffer julia-snail-repl-buffer)
		(f module-finder))))


(defun yank-from-minibuffer ()
  "Docu."
  (interactive)
  (with-current-buffer " *Minibuf-1*"
    (kill-new (buffer-string))))

(define-key minibuffer-mode-map (kbd "C-x y") #'yank-from-minibuffer)
(setq enable-recursive-minibuffers t) 	;TODO this seems useful

(require 'vertico-buffer)
(vertico-buffer-mode -1)

(require 'vertico-directory)

(straight-use-package 'catppuccin-theme)
(require 'catppuccin-theme)
(setq catppuccin-height-title-1 1.0)
(setq catppuccin-height-title-2 1.0)
(setq catppuccin-height-title-3 1.0)
(setq catppuccin-enlarge-headings nil)
(load-theme 'catppuccin t)


(straight-use-package 'polymode)
(require 'polymode)

(straight-use-package 'evil-multiedit)
(require 'evil-multiedit)

(evil-multiedit-default-keybinds)
(straight-use-package 'evil-mc)
(require 'evil-mc)

(straight-use-package '(emacs-chess :type git :host github
				    :repo "jwiegley/emacs-chess"))
(require 'chess)

;; benchmark init
(straight-use-package 'benchmark-init)
(require 'benchmark-init)


(straight-use-package 'esup)
(require 'esup)
(esup)

;; Optimistic require
(defun straight-optimistic-require (sym)
  "Like 'require SYM but first try to guess the correct load path.

This avoids the O(n) search used buy the built in required and in case of
failure uses the full 'load-path and the built in behaviour."
  (unless (ignore-errors
	    (let ((load-path (straight--build-dir (symbol-name sym))))
	      (require sym)))
    (require sym)))

(symbol-name 'org-bullets)

;; Org mode insert $
(define-key org-mode-map (kbd "$")
  (lambda ()
    (interactive)
    (if (= (char-before) ?$)
	(progn
	  (delete-char -1)
	  (insert "\\(\\)")
	  (backward-char)
	  (backward-char))
      (call-interactively #'self-insert-command))))

(setq org-todo-keywords '((sequence "TODO" "IMPORTANT TODO" "|" "DONE" )))


;; how to add stuff to the modeline
(setq global-mode-string
      '(:eval
	(format "%d " (+ 1 2))))


(defun capf-latex-fractions ()
  "Expand common fraction notation into latex."
  (interactive)
  (save-excursion
    (backward-word 2)
    (when (looking-at (rx (group (+ num)) (* whitespace) ?/ (group (+ num))))
      (list (match-beginning 0)
	    (match-end 0)
	    (let ((hash (make-hash-table)))
	      (puthash "1/2" "Hello" hash)
	      hash)
	    :exclusive 'no
	    :annotation-function
	    (lambda (_) " \\frac")
	    ))))

(defconst my-keywords '("1/2titus" "1/2talso" "1/2tmyne" "1/2tonemore" "1/2ds"))

(defun my-comint-dynamic-completion-function ()
  (when-let* ((bds (bounds-of-thing-at-point 'symbol))
              (beg (car bds))
              (end (cdr bds)))
    (when (> end beg)

      (list beg end my-keywords
	    :exclusive 'no
	    :annotation-function (lambda (_) " my-keywords")))))
(setq completion-at-point-functions nil)

(try-completion "1/2" (lambda (s _ __) "\\frac{1}{2}"))
(kill-new (rx (group (+ num)) (* whitespace) ?/ (group (+ num))))


(defun capf-latex-fractions ()
  "Expand common fraction notation into latex."
  (interactive)
  (message "Compliting")
  (save-excursion
    (backward-word 2)
    (when (looking-at (rx (group (+ num)) (* whitespace) ?/ (group (+ num))))
      (list (match-beginning 0)
	    (match-end 0)
	    (list (format "%s" (match-string 0)))
	    :exclusive 'no
	    :exit-function
	    (lambda (s _)
	      (delete-region (match-beginning 0) (match-end 0))
	      (insert (let ((parts (split-string s "/")))
			(format "\\frac{%s}{%s}" (car parts) (cadr parts)))))
	    :annotation-function
	    (lambda (s) (let ((parts (split-string s "/")))
		     (format "\\frac{%s}{%s}" (car parts) (cadr parts))))))))

(setq completion-at-point-functions nil)
(add-to-list 'completion-at-point-functions 'capf-latex-fractions)

;; 1/2

(try-completion "1/2" (lambda (s _ __) "\\frac{1}{2}"))
(try-completion "1/2" '("1/2" . "Help"))

(straight-use-package 'password-store)

(straight-use-package '(empv :host github :repo "isamert/empv.el"))
(require 'empv)
(setq empv-invidious-instance "https://iv.melmac.space/api/v1")

(defvar vterm-term-environment-variable)
(setq vterm-term-environment-variable "vterm")

(declare-function vterm-mode "vterm.el")
(advice-add
 #'vterm-mode :around
 (lambda (old &rest r)
   (let ((process-environment (append "$TERM_CHAN_FIFO=testing"
				      process-environment)))
     (apply old r))
   )) 				; TODO this doesn't work yet

;; (add-hook 'vterm-mode-hook
;; 	    (lambda () (setenv "TERM_CHAN_FIFO" "testing") ))
;;
(add-to-list 'magit-section-initial-visibility-alist '(untracked . show))

;; todo julia-staticlint
(provide 'scratch)
;;; scratch.el ends here
