#!/bin/sh

# user-defined pre-exec hook
test -r "${XDG_CONFIG_HOME:-$HOME/.config}"/pinentry/preexec &&
    . "${XDG_CONFIG_HOME:-$HOME/.config}"/pinentry/preexec

# site-defined pre-exec hook
test -r /etc/pinentry/preexec &&
    . /etc/pinentry/preexec


# if [ $TERM = "alacritty" ] || [ $TERM = "xterm-256color" ]; then
if [ $TERM = "alacritty" ]; then
    exec /usr/bin/pinentry-curses "$@"
else
    if [ $DESKTOP_SESSION = "gnome" -o $DESKTOP_SESSION = "ubuntu" ]; then
	exec /usr/bin/pinentry-gnome3 "$@"
    else
	exec /usr/bin/pinentry-dmenu "$@"
    fi
fi
