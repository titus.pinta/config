#! /bin/bash

# Read the username and the hostname
echo -n "Username: "
read username
echo "Username: " $username

echo -n "Host: "
read hostnae
echo "Host: " $username


# Timezone and locale configuration
ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
hwclock --systohc

printf "en_GB.UTF-8 UTF-8\nen_US.UTF-8 UTF-8\nfr_FR.UTF-8 UTF-8\n" > /etc/locale.gen
locale-gen

printf "LANG=en_GB.UTF-8\n" > /etc/locale.conf

printf "KEYMAP=us\n" > /etc/vconsole.conf

timedatectl

# Internet configuration
printf "${hostname}\n" > /etc/hostname

pacman --noconfirm -S networkmanager
systemctl start NetworkManager

printf "127.0.0.1\tlocalhost\n::1\tlocalhost ip6-localhost ip6-loopback\nff02::1\tip6-allnodes\nff02::2\tip6-allrouters\n" > /etc/hosts

# Setup arch mirrors list
pacman --noconfirm -S reflector
systemctl enable reflector

pacman -Fy

# Pacman configuration
sed -i "/\\[options\\]/a ParallelDownloads = 5\nILoveCandy\n" /etc/pacman.conf
sed -i "/#\\[multilib\\]\n#Include = \\/etc\\/pacman.d\\/mirrorlist/\\[multilib\\]\nInclude = \\/etc\\/pacman.d\\/mirrorlist/" /etc/pacman.conf


# Download basic tools and utils
pacman --noconfirm -S \
       base-devel \
       git cmake \
       vim neovim vi \
       wget \
       zsh

# File system tools
pacman --noconfirm -S btrfs-utils

# Setup superuser
pacman --noconfirm -S sudo doas
sed -i "/#%wheel ALL=(ALL:ALL) NOPASSWD: ALL/%wheel ALL=(ALL:ALL) NOPASSWD: ALL" /etc/sudoers

# Setup the bootloader
pacman --noconfirm -S grub efibootmgr
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

# User and passwd configurationo
passwd
useradd $username -m -G wheel
passwd $username
mkinitcpio -P

# Shell
pacman --noconfirm -S zsh
chsh -s /bin/zsh $username

# AUR
cd .local/share
[ -d yay-bin ] || git clone https://aur.archlinux.org/yay-bin.git
cd yay-bin
makepkg -si

# Crontab
pacman --noconfirm -S cronie
systemctl enable cronie

# Wayland and gnome
pacman --noconfirm -S gnome gdm
systemctl enable gdm


# GUI apps
pacman --noconfirm -S firefox xournalpp


# Programming tools
pacman --noconfirm -S libgccjit rustup emacs tree-sitter
echo 1 | yay --noconfirm juliaup-bin

rustup default nightly
juliaup add nightly
juliaup default nightly

# Usability tools
pacman --noconfirm -S tldr man-db
