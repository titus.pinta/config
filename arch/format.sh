#! /bin/bash

PS3="Select disk"
select disk in $(ls /sys/block)
do
    echo $disk
    break
done

echo "WARNING!! this will brick your pc!"
exit 1

mkfs.fat -F 32 ${disk}1
mkswap ${disk}2
mkfs.ext4 ${disk}3
mkfs.btrfs -L home ${disk}3
