function choose_wm() {
    choice=$( (dialog --menu "Choose the WM" 10 10 3\
	    1 "DWM" 2 "Xmonad" 3 "Gnome"\
	    3>&1 1>&2 2>&3 3>&-) || echo -1)
    case "$choice" in
	"1") export SESSION="dwm";;
	"2") export SESSION="xmonad";;
	"3") export SESSION="gnome";;
	*) export SESSION="zsh"; return 1;;
    esac
}

if [ $(tty) = "/dev/tty1" ]; then
    choose_wm
    case "$SESSION" in
	"gnome") sudo systemctl start gdm;;
	"zsh") ;;
	*) starx; export $SESSION="zsh"
    esac
else
    export SESSION="zsh"
fi
