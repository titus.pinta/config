zstyle :compinstall filname "$HOME/.zshrc"

autoload -Uz compinit
compinit

export HISTFILE="$HOME/.histfile"
export HISTSIZE=10000
export SAVEHIST=10000

setopt appendhistory autocd extendedglob correct

bindkey -v

autoload -Uz promptinit
promptinit

autoload edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line

autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )

setopt prompt_subst

PROMPT_ERROR_NO='%(?.%F{green}%?.%F{red}%?)%f'
PROMPT_USER='%F{cyan}%n%f'
PROMPT_PC='%F{magenta}%m%f'
PROMPT_FOLDER='%F{blue}%~%f'

PS1='%(!,%F{red}!%f ,)$PROMPT_ERROR_NO>$PROMPT_USER@$PROMPT_PC:$PROMPT_FOLDER${vcs_info_msg_0_}$PROMPT_SIGN%# '

zstyle ':vcs_info:git:*' formats ':%F{yellow}%b%f'

if [ $TERM = "dumb" ]; then
    unsetopt zle
    PS1='$ '
fi

RPS1="[%F{green}%T%f]"

if [ $SSH_CONNECTION ]; then
	RPS1="[%F{red}ssh%f] $RPS1"
fi

RPS1="%(!,[%F{red}root%f] ,)$RPS1"

if [ $INSIDE_EMACS ]; then
    export EDITOR=emacsclient
else
    export EDITOR=nvim
fi

export PAGER=slit
export MANPAGER=slit

if [ -f $HOME/.local/restic/pass ]; then
   export RESTIC_REPOSITORY=$HOME/Storage/Backups/Restic
   export RESTIC_PASSWORD=$(cat $HOME/.local/restic/pass)
fi

if [ -f /bin/eza-wrapper ]; then
    alias ls=eza-wrapper
else
    alias ls=eza
fi
alias sudo=doas
alias vim=nvim
alias less=slit
alias more=slit
alias emacsclient=emacsclient -n

export PATH=$PATH:$HOME/.local/bin/:$HOME/go/bin:$HOME/.ghcup/env
export GOBIN=$HOME/.local/go

[[ -r "$HOME/.local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" ]] && \
    source "$HOME/.local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"

[[ -r "$HOME/.config/broot/launcher/bash/br" ]] && \
    source "$HOME/.config/broot/launcher/bash/br"

[[ -r "/usr/share/z/z.sh" ]] && source /usr/share/z/z.sh
