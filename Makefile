VERSION=0.0.1
.PHONY: default
default:
	find ./ -mindepth 2 -type f -name Makefile -execdir $(MAKE) \;

.PHONY: install
install:
	find ./ -mindepth 2 -type f -name Makefile -execdir $(MAKE) install \;
