if [[ -n $TERM_CHAN_FIFO ]]; then
    disown
    mkfifo "/tmp/${TERM_CHAN_FIFO}_in"
    mkfifo "/tmp/${TERM_CHAN_FIFO}_out"
    msg=$(cat "/tmp/${TERM_CHAN_FIFO}_in")
    while [[ "${msg}" != "close_fifo" ]]; do
	eval "${msg}" > "/tmp/${TERM_CHAN_FIFO}_out"
	msg=$(cat "/tmp/${TERM_CHAN_FIFO}_in")
    done
fi
