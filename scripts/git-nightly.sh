#!/bin/bash

old_IFS=$IFS
IFS=$'\n'
for d in $(find / -name .git -type d -exec dirname {} \; 2> /dev/null | grep -v "\.local\|straight\|.cache\|tmp"); do
	cd $d
	echo $d

	current=$(git branch --show-current)
	# copy the current dirty branch
	echo "Check if current branch is clean"
	if ! git status | grep -q 'nothing to commit'; then
	    git stash push
	    git checkout -B "${current}-nightly" "${current}"
	    git stash apply
	    git add -A
	    git commit -m "Nightly auto-commit on dirty branch: $(date +%Y/%0m/%0d\ %T)"
	    git push --force origin "${current}-nightly"
	    git checkout "${current}"
	    git stash pop
	fi

	echo "Check if current branch is ahead of origin"
	if git status | grep -q 'is ahead of'; then
	    git push
	    git checkout -B "${current}-nightly" "${current}"
	    git push --force origin "${current}-nightly"
	fi


	# push all branches with commit
	echo "Check which branches are ahead of origin"
	for b in $(git branch | grep -v 'nightly\|\*' | sed 's/ //g'); do
	    echo "Checking status of branch ${b}"
	    git checkout "${b}"
	    if git status | grep -q 'is ahead of'; then
	    git checkout -B "${b}-nightly" "${b}"
		git push --force origin "${b}-nightly"
	    fi
	done

	git checkout "${current}"
done
IFS=$old_IFS
