#!/bin/bash

old_IFS=$IFS
IFS=$'\n'
not_clean=0
for d in $(find ~ -name .git -type d -exec dirname {} \; 2> /dev/null | grep -v "\.local\|straight\|.cache\|tmp"); do
    cd $d
    printf "%-50s " ~${d#$HOME}:
    # git fetch > /dev/null 2>&1

    git status | grep -q 'nothing to commit' && \
	printf "\033[0;32mclean    \033[0m" \
	    || (printf "\033[0;31mNOT clean\033[0m"; false)

    not_clean=$(($not_clean + $?))

    git status | grep -q 'ahead of' && printf "\t\033[0;31mAHEAD OF ORIGIN\033[0m"
    git status | grep -q 'behind' && printf "\t\033[0;31mBEHIND OF ORIGIN\033[0m"

    echo
done
IFS=old_IFS
exit $not_clean
