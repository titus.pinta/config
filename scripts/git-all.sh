#!/bin/bash

old_IFS=$IFS
IFS=$'\n'
for d in $(find ~ -name .git -type d -exec dirname {} \; 2> /dev/null | grep -v "\.local\|straight\|.cache\|tmp"); do
    cd $d
    echo ~${d#$HOME}:
    echo "git $@"
    git $@
done
IFS=old_IFS
