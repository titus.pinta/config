#! /bin/bash

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 URL" >&2
  exit 1
fi

if [ -b /dev/disk/by-label/MAIN-KEY ]; then
    BASE_DIR=$(lsblk /dev/disk/by-label/MAIN-KEY --json | jshon -e blockdevices -e 0 -e mountpoints -e 0 -u)

    cat ~/.ssh/id_*.pub > /tmp/authorized_keys

    gpg2 --import $BASE_DIR/main-key.asc
    gpg2 --decrypt --output /tmp/id $BASE_DIR/id.gpg
    chmod 600 /tmp/id

    scp -i /tmp/id /tmp/authorized_keys $1:/tmp/authorized_keys
    ssh -i /tmp/id $1 'cp ~/.ssh/authorized_keys ~/.ssh/authorized_keys.backup$(date +%F)'
    ssh -i /tmp/id $1 'cat /tmp/authorized_keys >> ~/.ssh/authorized_keys'
    ssh -i /tmp/id $1 'rm /tmp/authorized_keys'
fi
