[Unit]
Description=Kmonad; service for remapping keys

[Service]
ExecStart=/bin/kmonad syscmd(printf ~)/.config/kmonad/emacs.kbd

[Install]
WantedBy=multi-user.target
