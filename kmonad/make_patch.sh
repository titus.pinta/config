#!/bin/bash

PS3="Select keyboar for KMonad: "
select filename in $(ls /dev/input/by-path | grep kbd)
do
    echo $filename
    echo "--- emacs.kbd.old	2024-04-21 21:58:51.191978029 +0200" > build/emacs.kbd.patch
    echo "+++ emacs.kbd	2024-04-21 22:00:06.122774697 +0200" >> build/emacs.kbd.patch
    echo "@@ -2,7 +2,7 @@" >> build/emacs.kbd.patch
    echo " ;;    				 :repo \"kmonad/kbd-mode\"))" >> build/emacs.kbd.patch
    echo " "  >> build/emacs.kbd.patch
    echo " (defcfg" >> build/emacs.kbd.patch
    echo "-  input  (device-file \"/dev/input/by-path/USE_YOURS\")" >> build/emacs.kbd.patch
    echo "+  input  (device-file \"/dev/input/by-path/$filename\")" >> build/emacs.kbd.patch
    echo "   output (uinput-sink \"My KMonad output\"))" >> build/emacs.kbd.patch
    echo " "  >> build/emacs.kbd.patch
    echo " "  >> build/emacs.kbd.patch
    break
done
