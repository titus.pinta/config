.PHONY: default
default: install

.PHONY: install
install: ~/.config/kmonad/emacs.kbd


~/.config/kmonad/emacs.kbd: build/emacs.kbd
	mkdir -p ~/.config/kmonad
	cp build/emacs.kbd ~/.config/kmonad/emacs.kbd

build/emacs.kbd: emacs.kbd build/emacs.kbd.patch
	mkdir -p build
	cp emacs.kbd build/emacs.kbd
	cd build; patch --forward --reject-file=- < emacs.kbd.patch

/usr/bin/kmonad_start:
	printf "#!/bin/bash\nsudo -E kmonad ~/.config/kmonad/emacs.kbd & disown\n" | sudo tee /usr/bin/kmonad_start
	sudo chmod 755 /usr/bin/kmonad_start


.PHONY: test
test: ~/.config/kmonad/emacs.kbd
	sudo killall kmonad || true
	sudo kmonad ~/.config/kmonad/emacs.kbd

.PHONY: mkpatch
mkpatch:
	mkdir -p build
	./make_patch.sh

.PHONY: exec
exec: /bin/kmonad

/bin/kmonad:
	wget -qO- https://get.haskellstack.org/ | sh || true
	curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | sh || true
	~/.ghcup/bin/cabal install kmonad
	chmod 755 ~/.local/bin/kmonad
	sudo chown root:root ~/.local/bin/kmonad
	sudo cp ~/.local/bin/kmonad /bin/kmonad

/etc/systemd/system/kmonad.service: kmonad.service.m4
	mkdir -p build
	m4 kmonad.service.m4 > build/kmonad.service
	sudo cp build/kmonad.service /etc/systemd/system
	rm build/kmonad.service
	sudo chown root:root /etc/systemd/system/kmonad.service
	sudo chmod 644 /etc/systemd/system/kmonad.service
	sudo systemctl daemon-reload
